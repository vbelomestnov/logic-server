import { Test, TestingModule } from '@nestjs/testing';
import { GraphService, graphStubData } from './graph.service';
import * as mongoose from 'mongoose';
import { GraphCodeSequenceSchema } from '../mongoose/graph-code-sequence.schema';
import { GraphSchema } from '../mongoose/graph.schema';
import { getModelToken } from '@nestjs/mongoose';
import { GraphCodeSequenceService } from './graph-code-sequence.service';
import { Model } from 'mongoose';
import { GraphCodeSequence } from './types/graph-code-sequence.interface';
import { GraphDto } from '../dto/graph/graph.dto';
import { HttpStatus } from '@nestjs/common';
import { GRAPH_LIST_STUB, GraphShortDto } from '../dto/graph/graph.short.dto';
import { GraphCalcClientDataService } from '../graph-calc/graph-calc-client-data.service';
import { promises as fs } from 'fs';
import { GraphMakingLogSchema } from '../mongoose/graph-making-log.schema';
import { GraphMakingLogService } from './graph-making-log.service';
import { GraphMakingLog } from './types/graph-making-log.interface';

const GraphCodeSequenceModel = mongoose.model('GraphCodeSequence', GraphCodeSequenceSchema);
const GraphMakingLogModel = mongoose.model('GraphMakingLog', GraphMakingLogSchema);
const GraphModel = mongoose.model('Graph', GraphSchema);

describe('GraphService', () => {
  let service: GraphService;
  let codeSequenceModel: Model<GraphCodeSequence>;
  let makingLogModel: Model<GraphMakingLog>;
  let graphModel: Model<GraphDto>;
  let calcClientDataService: GraphCalcClientDataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GraphService,
        GraphCodeSequenceService,
        GraphMakingLogService,
        GraphCalcClientDataService,
        {
          provide: getModelToken('GraphCodeSequence'),
          useValue: GraphCodeSequenceModel,
        },
        {
          provide: getModelToken('GraphMakingLog'),
          useValue: GraphMakingLogModel,
        },
        {
          provide: getModelToken('Graph'),
          useValue: GraphModel,
        },
        { provide: 'WORKER', useValue: WorkerTesting },
      ],
    }).compile();

    service = module.get<GraphService>(GraphService);
    codeSequenceModel = module.get<Model<GraphCodeSequence>>(getModelToken('GraphCodeSequence'));
    makingLogModel = module.get<Model<GraphMakingLog>>(getModelToken('GraphMakingLog'));
    graphModel = module.get<Model<GraphDto>>(getModelToken('Graph'));
    calcClientDataService = module.get<GraphCalcClientDataService>(GraphCalcClientDataService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('saveGraph', async () => {
    const response: GraphDto = graphStubData();
    const spyCodeSequenceFindOne = jest.spyOn(codeSequenceModel, 'findOne').mockImplementation(() => Promise.resolve({ id: '123', code: '2a' }));
    const spyCodeSequenceyUpdate = jest.spyOn(codeSequenceModel, 'updateOne').mockImplementation(() => Promise.resolve());
    const spyMakingLogFind = jest.spyOn(makingLogModel, 'find').mockImplementation(countImpl);
    const spyMakingLogCreateFailure = jest.spyOn(makingLogModel, 'create').mockImplementation(() => Promise.reject('Test error'));
    const spyGraphCreate = jest.spyOn(graphModel, 'create').mockImplementation(() => Promise.resolve(response));
    const spyFsAccess = jest.spyOn(fs, 'access').mockImplementation(() => Promise.reject());
    const spyFsMkdir = jest.spyOn(fs, 'mkdir').mockImplementation(() => Promise.resolve());
    const spyFsWriteFile = jest.spyOn(fs, 'writeFile').mockImplementation(() => Promise.resolve());

    //There isn't client with socketId="123" in "calcClientDataService"
    //Expected error "There isn\'t initialized graph for client connection"
    try {
      await service.saveGraph('123', '<svg></svg>', '::1');
      fail('Unexpected behavior');
    } catch (e) {
      expect(spyCodeSequenceFindOne).not.toHaveBeenCalled();
      expect(spyCodeSequenceyUpdate).not.toHaveBeenCalled();
      expect(spyGraphCreate).not.toHaveBeenCalled();
      expect(spyMakingLogFind).not.toHaveBeenCalled();
      expect(e.status).toBe(HttpStatus.BAD_REQUEST);
    }

    calcClientDataService.data['123'] = {
      socket: null,
      graph: graphStubData(),
    };

    //Check for SHORT_TIME_LIMIT
    try {
      COUNT1 = 8;
      await service.saveGraph('123', '<svg></svg>', '::1');
      fail('Unexpected behavior');
    } catch (e) {
      expect(spyCodeSequenceFindOne).not.toHaveBeenCalled();
      expect(spyCodeSequenceyUpdate).not.toHaveBeenCalled();
      expect(spyGraphCreate).not.toHaveBeenCalled();
      expect(spyMakingLogFind).toHaveBeenCalled();
      expect(e.status).toBe(HttpStatus.TOO_MANY_REQUESTS);
    }

    //Check for LONG_TIME_LIMIT
    try {
      COUNT1 = 1;
      COUNT2 = 30;
      await service.saveGraph('123', '<svg></svg>', '::1');
      fail('Unexpected behavior');
    } catch (e) {
      expect(spyCodeSequenceFindOne).not.toHaveBeenCalled();
      expect(spyCodeSequenceyUpdate).not.toHaveBeenCalled();
      expect(spyGraphCreate).not.toHaveBeenCalled();
      expect(spyMakingLogFind).toHaveBeenCalledTimes(3);
      expect(e.status).toBe(HttpStatus.TOO_MANY_REQUESTS);
    }
    COUNT2 = 1;


    //Check if return error when "graphMakingLog.log" raise error
    try {
      await service.saveGraph('123', '<svg></svg>', '::1');
      fail('Unexpected behavior');
    } catch (e) {
      expect(spyGraphCreate).not.toHaveBeenCalled();
      expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    spyMakingLogCreateFailure.mockClear();

    //Check "saveSvg"
    const spyMakingLogCreate = jest.spyOn(makingLogModel, 'create').mockImplementation(() => Promise.resolve());
    const result = await service.saveGraph('123', '<svg></svg>', '::1');
    expect(spyCodeSequenceFindOne).toHaveBeenCalled();
    expect(spyCodeSequenceyUpdate).toHaveBeenCalled();
    expect(spyGraphCreate).toHaveBeenCalled();
    expect(spyFsAccess).toHaveBeenCalled();
    expect(spyFsWriteFile).toHaveBeenCalled();
    expect(spyFsMkdir).toHaveBeenCalled();
    expect(result).toBe(response);
    spyFsAccess.mockClear();

    const spyFsAccessResolve = jest.spyOn(fs, 'access').mockImplementation(() => Promise.resolve());
    await service.saveGraph('123', '<svg></svg>', '::1');
    expect(spyFsAccessResolve).toHaveBeenCalled();

    expect(spyFsMkdir).toHaveBeenCalledTimes(6);
    expect(spyFsWriteFile).toHaveBeenCalledTimes(3);

    spyGraphCreate.mockClear();

    //Handle error of graphModel
    const spyGraphCreateError = jest.spyOn(graphModel, 'create').mockImplementation(() => Promise.reject(new Error('Test error')));
    try {
      await service.saveGraph('123', '<svg></svg>', '::1');
    } catch (e) {
      expect(e.message).toBe('DB error has been occured while creating new Graph');
      expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    expect(spyGraphCreateError).toHaveBeenCalled();
  });

  it('getGraph', async () => {
    const response: GraphDto = graphStubData();
    const spyGraphFindOne = jest.spyOn(graphModel, 'findOne').mockImplementation(() => ({ lean: () => Promise.resolve(response) }));
    const result = await service.getGraph('2a');
    expect(spyGraphFindOne).toHaveBeenCalled();
    expect(result).toBe(response);
    spyGraphFindOne.mockClear();

    const newGraphResult = await service.getGraph('new');
    expect(spyGraphFindOne).not.toHaveBeenCalled();
    expect(newGraphResult.title).toBe('');
    spyGraphFindOne.mockClear();

    const spyGraphFindOneEmpty = jest.spyOn(graphModel, 'findOne').mockImplementation(() => ({ lean: () => Promise.resolve() }));
    try {
      await service.getGraph('2a');
    } catch (e) {
      expect(e.message).toBe('Graph not found 2a');
      expect(e.status).toBe(HttpStatus.BAD_REQUEST);
    }
    expect(spyGraphFindOneEmpty).toHaveBeenCalled();
    spyGraphFindOneEmpty.mockClear();

    const spyGraphFindOneError = jest.spyOn(graphModel, 'findOne')
      .mockImplementation(() => ({ lean: () => Promise.reject(new Error('Test error')) }));
    try {
      await service.getGraph('2a');
    } catch (e) {
      expect(e.message).toBe('DB error has been occured while creating new Graph');
      expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    expect(spyGraphFindOneError).toHaveBeenCalled();
    spyGraphFindOneError.mockClear();
  });

  it('getList', async () => {
    const response: GraphShortDto[] = GRAPH_LIST_STUB;
    let filterUsed = null;
    const spyGraphFind = jest.spyOn(graphModel, 'find').mockImplementation(
      (filter) => {
        filterUsed = filter;
        return { sort: () => Promise.resolve(response) };
      },
    );
    let result = await service.getGraphList({ search: null, skip: 0 });
    expect(spyGraphFind).toHaveBeenCalledTimes(1);
    expect(filterUsed).toEqual({ archived: false });
    expect(result).toBe(response);
    result = await service.getGraphList({ search: '2a', skip: 0 });
    expect(spyGraphFind).toHaveBeenCalledTimes(2);
    expect(result).toBe(response);
    expect(filterUsed).toEqual({ $text: { $search: '2a' }, archived: false });
    spyGraphFind.mockClear();

    const spyGraphFindError = jest.spyOn(graphModel, 'find').mockImplementation(() => Promise.reject(new Error('Test error')));
    try {
      await service.getGraphList({ search: null, skip: 0 });
    } catch (e) {
      expect(e.message).toBe('DB error has been occured while getting list of Graphs');
      expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    expect(spyGraphFindError).toHaveBeenCalled();
    spyGraphFindError.mockClear();
  });

  it('reviewGraph', async () => {
    const spyGraphUpdateOne = jest.spyOn(graphModel, 'updateOne').mockImplementation(() => Promise.resolve({ n: 1 }));
    const result = await service.reviewGraph('2a', true);
    expect(spyGraphUpdateOne).toHaveBeenCalled();
    expect(result).toEqual(true);
    spyGraphUpdateOne.mockClear();

    const spyGraphUpdateOneNil = jest.spyOn(graphModel, 'updateOne').mockImplementation(() => Promise.resolve({ n: 0 }));
    try {
      await service.reviewGraph('2a', true);
      fail('unexpected behavior');
    } catch (e) {
      expect(e.message).toBe('Graph not found 2a');
      expect(e.status).toBe(HttpStatus.BAD_REQUEST);
    }
    expect(spyGraphUpdateOneNil).toHaveBeenCalled();
    spyGraphUpdateOneNil.mockClear();

    const spyGraphUpdateOneError = jest.spyOn(graphModel, 'updateOne')
      .mockImplementation(() => Promise.reject(new Error('Test error')));
    try {
      await service.reviewGraph('2a', true);
      fail('unexpected behavior');
    } catch (e) {
      expect(e.message).toBe('DB error has been occured while reviewing a Graph');
      expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    expect(spyGraphUpdateOneError).toHaveBeenCalled();
    spyGraphUpdateOneError.mockClear();
  });

  it('archiveGraph', async () => {
    const spyGraphUpdateOne = jest.spyOn(graphModel, 'updateOne').mockImplementation(() => Promise.resolve({ n: 1 }));
    const result = await service.archiveGraph('2a');
    expect(spyGraphUpdateOne).toHaveBeenCalled();
    expect(result).toEqual(true);
    spyGraphUpdateOne.mockClear();

    const spyGraphUpdateOneNil = jest.spyOn(graphModel, 'updateOne').mockImplementation(() => Promise.resolve({ n: 0 }));
    try {
      await service.archiveGraph('2a');
      fail('unexpected behavior');
    } catch (e) {
      expect(e.message).toBe('Graph not found 2a');
      expect(e.status).toBe(HttpStatus.BAD_REQUEST);
    }
    expect(spyGraphUpdateOneNil).toHaveBeenCalled();
    spyGraphUpdateOneNil.mockClear();

    const spyGraphUpdateOneError = jest.spyOn(graphModel, 'updateOne')
      .mockImplementation(() => Promise.reject(new Error('Test error')));
    try {
      await service.archiveGraph('2a');
      fail('unexpected behavior');
    } catch (e) {
      expect(e.message).toBe('DB error has been occured while archiving a Graph');
      expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    expect(spyGraphUpdateOneError).toHaveBeenCalled();
    spyGraphUpdateOneError.mockClear();
  });
});

class WorkerTesting {
  /* tslint:disable:no-empty */
  on(message: string, callback: (data: any) => {}) { }
  terminate() { }
}

let COUNT1 = 1;
let COUNT2 = 2;
const countImpl = (filter: any) => {
  if (filter.$and[0].$expr.$lte[1] <= (30 * 60 * 1000)) {
    return { countDocuments: () => Promise.resolve(COUNT1) };
  } else {
    return { countDocuments: () => Promise.resolve(COUNT2) };
  }
};
