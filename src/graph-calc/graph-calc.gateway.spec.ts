import { Test, TestingModule } from '@nestjs/testing';
import { GraphCalcGateway } from './graph-calc.gateway';
import { GraphCalcClientDataService } from './graph-calc-client-data.service';
import { GraphService, graphStubData } from '../graph/graph.service';
import { getModelToken } from '@nestjs/mongoose';
import { GraphSchema } from '../mongoose/graph.schema';
import { GraphCodeSequenceSchema } from '../mongoose/graph-code-sequence.schema';
import * as mongoose from 'mongoose';
import { GraphCodeSequenceService } from '../graph/graph-code-sequence.service';
import { listen, Socket } from 'socket.io';
import { connect } from 'socket.io-client';
import { GraphDto } from '../dto/graph/graph.dto';
import { CalcTask } from './types/calc-task.interface';
import { Link } from 'src/dto/shapes/link';
import { RemoveLinkParams } from './types/remove-link-params.interface';
import { SaveShapeTextParams } from './types/save-shape-text-params';
import { GraphMakingLogService } from '../graph/graph-making-log.service';
import { GraphMakingLogSchema } from '../mongoose/graph-making-log.schema';

const GraphCodeSequence = mongoose.model('GraphCodeSequence', GraphCodeSequenceSchema);
const GraphMakingLogModel = mongoose.model('GraphMakingLog', GraphMakingLogSchema);
const Graph = mongoose.model('Graph', GraphSchema);

describe('GraphCalcGateway', () => {
  let gateway: GraphCalcGateway;
  let client;
  let calcDataService: GraphCalcClientDataService;
  let graphService: GraphService;
  let server;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GraphService,
        GraphCodeSequenceService,
        GraphMakingLogService,
        GraphCalcGateway,
        GraphCalcClientDataService,
        {
          provide: getModelToken('GraphCodeSequence'),
          useValue: GraphCodeSequence,
        },
        {
          provide: getModelToken('GraphMakingLog'),
          useValue: GraphMakingLogModel,
        },
        {
          provide: getModelToken('Graph'),
          useValue: Graph,
        },
        { provide: 'WORKER', useValue: WorkerTesting },
      ],
    }).compile();

    gateway = module.get<GraphCalcGateway>(GraphCalcGateway);
    calcDataService = module.get<GraphCalcClientDataService>(GraphCalcClientDataService);
    graphService = module.get<GraphService>(GraphService);

    server = listen(3001);
    client = connect('http://localhost:3001', {});
  });

  afterEach(async () => {
    if (client && client.connected) {
      client.disconnect();
      client.close();
    }
    server.close();
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });

  it('should save client after socket connection', (done) => {
    server.on('connection', async (socket: Socket) => {
      gateway.handleConnection(socket);
      expect(calcDataService.data[socket.id]).toBeTruthy();
      gateway.handleDisconnect(socket);
      expect(calcDataService.data[socket.id]).toBeFalsy();
      done();
    });
  });

  it('should init graph for further calculations', async (done) => {
    const result: GraphDto = graphStubData();
    server.on('connection', async (socket: Socket) => {
      const spy = jest.spyOn(graphService, 'getGraph').mockImplementation(() => Promise.resolve(result));
      await gateway.handleInitGraph('2a', socket);
      expect(spy).toHaveBeenCalled();
      spy.mockClear();

      await gateway.handleConnection(socket);
      await gateway.handleInitGraph('2a', socket);
      expect(spy).toHaveBeenCalled();
      spy.mockClear();

      spy.mockImplementation(() => Promise.reject(new Error('Test error')));

      try {
        await gateway.handleInitGraph('2a', socket);
      } catch (e) {
        expect(e.message).toBeTruthy();
      }

      done();
    });
  });

  it('should handle "addShape" message', (done) => {
    const graph: GraphDto = graphStubData();
    const newShape = {
      ...graph.shapes[3],
      name: 'object6',
    };
    const spy = jest.spyOn(calcDataService, 'addTask');

    server.on('connection', async (socket: Socket) => {
      calcDataService.addClient(socket);
      calcDataService.initGraph(socket, graph);
      await gateway.handleAddShape(newShape, socket);
      expect(spy).toHaveBeenNthCalledWith(1, CalcTask.addShape, newShape, socket);

      spy.mockClear();
      spy.mockImplementation(() => { throw new Error('Test error'); });
      try {
        await gateway.handleAddShape(newShape, socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toBeTruthy();
      }
      done();
    });
  });

  it('should handle "removeShape" message', async (done) => {
    const graph: GraphDto = graphStubData();
    const spy = jest.spyOn(calcDataService, 'addTask');

    server.on('connection', async (socket: Socket) => {
      calcDataService.addClient(socket);
      calcDataService.initGraph(socket, graph);
      await gateway.handleRemoveShape(graph.shapes[0].name, socket);
      expect(spy).toHaveBeenNthCalledWith(1, CalcTask.removeShape, graph.shapes[0].name, socket);

      spy.mockClear();
      spy.mockImplementation(() => { throw new Error('Test error'); });
      try {
        await gateway.handleRemoveShape(graph.shapes[0].name, socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toBeTruthy();
      }
      done();
    });
  });

  it('should handle "addLink" message', async (done) => {
    const graph: GraphDto = graphStubData();
    const newLink: Link = {
      source: graph.shapes[0].name,
      dest: graph.shapes[0].name,
      translation: null,
      rotation: 0,
      length: 0,
      route: null,
    };
    const spy = jest.spyOn(calcDataService, 'addTask');

    server.on('connection', async (socket: Socket) => {
      calcDataService.addClient(socket);
      calcDataService.initGraph(socket, graph);
      await gateway.handleAddLink(newLink, socket);
      expect(spy).toHaveBeenNthCalledWith(1, CalcTask.addLink, newLink, socket);

      spy.mockClear();
      spy.mockImplementation(() => { throw new Error('Test error'); });
      try {
        await gateway.handleAddLink(newLink, socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toBeTruthy();
      }
      done();
    });
  });

  it('should handle "removeLink" message', async (done) => {
    const graph: GraphDto = graphStubData();
    const removeLinkParams: RemoveLinkParams = {
      source: graph.shapes[0].name,
      dest: graph.shapes[1].name,
    };
    const spy = jest.spyOn(calcDataService, 'addTask');

    server.on('connection', async (socket: Socket) => {
      calcDataService.addClient(socket);
      calcDataService.initGraph(socket, graph);
      await gateway.handleRemoveLink(removeLinkParams, socket);
      expect(spy).toHaveBeenNthCalledWith(1, CalcTask.removeLink, removeLinkParams, socket);

      spy.mockClear();
      spy.mockImplementation(() => { throw new Error('Test error'); });
      try {
        await gateway.handleRemoveLink(removeLinkParams, socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toBeTruthy();
      }
      done();
    });
  });

  it('should handle "setShapePlacements" message', async (done) => {
    const graph: GraphDto = graphStubData();
    const shape = {
      ...graph.shapes[3],
      name: 'object6',
    };
    const spy = jest.spyOn(calcDataService, 'addTask');

    server.on('connection', async (socket: Socket) => {
      calcDataService.addClient(socket);
      calcDataService.initGraph(socket, graph);
      await gateway.handleSetShapePlacement(shape, socket);
      expect(spy).toHaveBeenNthCalledWith(1, CalcTask.setShapePlacements, shape, socket);

      spy.mockClear();
      spy.mockImplementation(() => { throw new Error('Test error'); });
      try {
        await gateway.handleSetShapePlacement(shape, socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toBeTruthy();
      }
      done();
    });
  });

  it('should handle "saveTitle" message', async (done) => {
    const graph: GraphDto = graphStubData();
    const newTitle = 'NEW TITLE';
    const spy = jest.spyOn(calcDataService, 'addTask');

    server.on('connection', async (socket: Socket) => {
      calcDataService.addClient(socket);
      calcDataService.initGraph(socket, graph);
      await gateway.handleSaveTitle(newTitle, socket);
      expect(spy).toHaveBeenNthCalledWith(1, CalcTask.saveTitle, newTitle, socket);

      spy.mockClear();
      spy.mockImplementation(() => { throw new Error('Test error'); });
      try {
        await gateway.handleSaveTitle(newTitle, socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toBeTruthy();
      }
      done();
    });
  });

  it('should handle "saveShapeText" message', async (done) => {
    const graph: GraphDto = graphStubData();
    const saveShapeTextParams: SaveShapeTextParams = {
      shapeName: graph.shapes[0].name,
      text: 'New Text',
    };
    const spy = jest.spyOn(calcDataService, 'addTask');

    server.on('connection', async (socket: Socket) => {
      calcDataService.addClient(socket);
      calcDataService.initGraph(socket, graph);
      await gateway.handleSaveShapeText(saveShapeTextParams, socket);
      expect(spy).toHaveBeenNthCalledWith(1, CalcTask.saveShapeText, saveShapeTextParams, socket);

      spy.mockClear();
      spy.mockImplementation(() => { throw new Error('Test error'); });
      try {
        await gateway.handleSaveShapeText(saveShapeTextParams, socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toBeTruthy();
      }
      done();
    });
  });

  it('should handle "break" message', async (done) => {
    const graph: GraphDto = graphStubData();
    const spy = jest.spyOn(calcDataService, 'breakCalculations');

    server.on('connection', async (socket: Socket) => {
      calcDataService.addClient(socket);
      calcDataService.initGraph(socket, graph);
      await gateway.handleBreakCalculatoins(socket);
      expect(spy).toHaveBeenNthCalledWith(1, socket);

      spy.mockClear();
      spy.mockImplementation(() => { throw new Error('Test error'); });
      try {
        await gateway.handleBreakCalculatoins(socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toBeTruthy();
      }
      done();
    });
  });
});

class WorkerTesting {
  /* tslint:disable:no-empty */
  on(message: string, callback: (data: any) => {}) { }
  terminate() { }
}
