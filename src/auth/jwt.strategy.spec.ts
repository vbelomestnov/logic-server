import { Test, TestingModule } from '@nestjs/testing';
import * as mongoose from 'mongoose';
import { UserSchema } from '../mongoose/user.schema';
import { JwtStrategy } from './jwt.strategy';

const User = mongoose.model('User', UserSchema);

describe('JwtStrategy', () => {
  let service: JwtStrategy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
      ],
      providers: [
        JwtStrategy,
      ],
    }).compile();

    service = module.get<JwtStrategy>(JwtStrategy);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should validate', async () => {
    expect(await service.validate({ sub: 'id', login: 'test' })).toEqual({ _id: 'id', login: 'test' });
  });
});
