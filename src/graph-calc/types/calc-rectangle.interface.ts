import { Rectangle } from '../../dto/shapes/rectangle';
import { DimensionLine } from './dimension-line.interface';

export interface CalcRectangle extends Rectangle {
  placement: DimensionLine[];
  selfPlacement: DimensionLine[];
  isCovered: boolean;
}
