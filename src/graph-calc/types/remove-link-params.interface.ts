export interface RemoveLinkParams {
  source: string;
  dest: string;
}
