export enum WorkerMessage {
  graph,
  exception,
  outOfBoard,
  close,
}
