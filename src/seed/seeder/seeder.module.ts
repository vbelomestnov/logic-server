import { Module, Logger } from '@nestjs/common';
import { SeederService } from './seeder.service';
import { UserModule } from '../../user/user.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/nest_test'),
    UserModule,
  ],
  providers: [SeederService, Logger],
})
export class SeederModule { }
