import { GraphCalc } from './graph-calc';
import { graphStubData, graphStubData2 } from '../graph/graph.service';
import { Link } from 'src/dto/shapes/link';
import { GraphDto } from 'src/dto/graph/graph.dto';

describe('GraphCalcGateway', () => {
  let calc: GraphCalc = null;
  beforeEach(async () => {
    calc = new GraphCalc(graphStubData());
  });

  it('should build link between two shapes', () => {
    const graph = graphStubData2();
    calc = new GraphCalc(graph);
    const shape = {
      ...graph.shapes[0],
    };
    const newGraph = calc.setShapePlacements(shape);
    expect(newGraph.links[0].length).toBeGreaterThan(0);
    expect(newGraph.links[0].route.length).toBe(0);
  });

  it('should add new shape in a graph', () => {
    const graph = graphStubData();
    const newShape = {
      ...graph.shapes[3],
      name: 'object6',
    };
    let newGraph;
    try {
      newGraph = calc.addShape(newShape);
      fail('Unexpected behavior');
    } catch (e) {
      expect(e.message).toBeTruthy();
    }
    newShape.x = 259;
    newShape.y = 296;
    newGraph = calc.addShape(newShape);
    expect(newGraph.shapes.length).toBe(6);
  });

  it('should remove shape from a graph', () => {
    const graph = graphStubData();
    const newGraph = calc.removeShape(graph.shapes[2].name);
    expect(newGraph.shapes.length).toBe(4);
    expect(newGraph.links.length).toBe(2);
    expect(newGraph.links[1].route.length).toBe(0);
  });

  it('should add new link in a graph', () => {
    const graph: GraphDto = graphStubData();
    const newLink: Link = {
      source: graph.shapes[0].name,
      dest: graph.shapes[0].name,
      translation: null,
      rotation: 0,
      length: 0,
      route: null,
    };
    let newGraph;
    try {
      newGraph = calc.addLink(newLink);
      fail('Unexpected behavior');
    } catch (e) {
      expect(e.message).toBeTruthy();
    }
    newLink.dest = graph.shapes[2].name;
    try {
      newGraph = calc.addLink(newLink);
      fail('Unexpected behavior');
    } catch (e) {
      expect(e.message).toBeTruthy();
    }
    newLink.dest = graph.shapes[1].name;
    newGraph = calc.addLink(newLink);
    expect(newGraph.links.length).toBe(4);
  });

  it('should remove link from a graph', async () => {
    const graph: GraphDto = graphStubData();
    let newGraph;
    newGraph = calc.removeLink(graph.shapes[1].name, graph.shapes[0].name);
    expect(newGraph.links.length).toBe(2);
    newGraph = calc.removeLink(graph.shapes[1].name, graph.shapes[0].name);
    expect(newGraph.links.length).toBe(2);
  });

  it('should set shape placement', () => {
    const graph: GraphDto = graphStubData();
    const shape = {
      ...graph.shapes[0],
    };
    let newGraph;

    // Rect 1 covers rect3
    shape.x = 109;
    shape.y = 164;
    try {
      newGraph = calc.setShapePlacements(shape);
      fail('Unexpected behavior');
    } catch (e) {
      expect(e.message).toBeTruthy();
    }

    // Rect 1 out of board
    shape.x = 265;
    shape.y = 33;
    try {
      newGraph = calc.setShapePlacements(shape);
      fail('Unexpected behavior');
    } catch (e) {
      expect(e.message).toBeTruthy();
      expect(calc.isOutOfBoard).toBeTruthy();
    }

    // Rect 3 inside rect 1
    shape.x = 31;
    shape.y = 193;
    try {
      newGraph = calc.setShapePlacements(shape);
      fail('Unexpected behavior');
    } catch (e) {
      expect(e.message).toBeTruthy();
    }

    shape.x = 148;
    shape.y = 56;

    // Rect 1 has short dimensions
    shape.height = 4;
    shape.width = 4;
    try {
      newGraph = calc.setShapePlacements(shape);
      fail('Unexpected behavior');
    } catch (e) {
      expect(e.message).toBeTruthy();
    }

    // Correct input data
    shape.height = 25;
    shape.width = 25;
    newGraph = calc.setShapePlacements(shape);
    const changedShape = newGraph.shapes.find(aShape => aShape.name === shape.name);
    expect(changedShape.x).toBe(148);
    expect(changedShape.y).toBe(56);
  });

  it('should save title', () => {
    const NEW_TITLE = 'New title';
    const newGraph = calc.saveTitle(NEW_TITLE);
    expect(newGraph.title).toBe(NEW_TITLE);
  });

  it('should save shape text', () => {
    const NEW_TEXT = 'New text';
    calc.saveShapeText(calc.graph.shapes[0].name, NEW_TEXT);
    expect(calc.graph.shapes[0].text).toBe(NEW_TEXT);
  });
});
