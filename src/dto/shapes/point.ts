import { IsNumber, Min } from 'class-validator';

export class Point {
    @IsNumber()
    @Min(0)
    x: number;

    @IsNumber()
    @Min(0)
    y: number;
}
