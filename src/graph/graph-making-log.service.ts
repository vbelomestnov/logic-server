import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { GraphMakingLog } from './types/graph-making-log.interface';
import { Model } from 'mongoose';

@Injectable()
export class GraphMakingLogService {

  constructor(
    @InjectModel('GraphMakingLog') private readonly graphMakingLogModel: Model<GraphMakingLog>,
  ) { }

  async log(code: string, ipAddress: string): Promise<GraphMakingLog> {
    try {
      const newLog = await this.graphMakingLogModel.create({
        code,
        ipAddress,
        createdDate: new Date(),
      });
      return newLog;
    } catch (e) {
      console.error(e);
      throw new HttpException('DB error has been occured while logging new Graph', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async checkTimeLimitCount(ipAddress: string, milliseconds: number) {
    const nowDate = new Date();
    return await this.graphMakingLogModel
      .find({
        $and: [
          {
            $expr: {
              $lte: [{ $subtract: [new Date(), "$createdDate"] }, milliseconds]
            }
          },
          { ipAddress }
        ]
      })
      .countDocuments();
  }
}
