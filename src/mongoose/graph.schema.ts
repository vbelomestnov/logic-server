import * as mongoose from 'mongoose';

export const GraphSchema = new mongoose.Schema({
  code: String,
  title: String,
  width: Number,
  height: Number,
  shapes: [{
    code: String,
    name: String,
    text: String,
    type: { type: Number },
    height: Number,
    width: Number,
    x: Number,
    y: Number,
  }],
  links: [{
    source: String,
    dest: String,
    translation: {
      x: Number,
      y: Number,
    },
    rotation: Number,
    length: Number,
    route: [[{
      x: Number,
      y: Number,
    }]],
  }],
  approved: Boolean,
  archived: Boolean,
});

GraphSchema.index({
  code: 'text',
  title: 'text',
});
