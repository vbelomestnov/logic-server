import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GraphCodeSequence } from './types/graph-code-sequence.interface';

@Injectable()
export class GraphCodeSequenceService {
  constructor(
    @InjectModel('GraphCodeSequence') private readonly graphCodeModel: Model<GraphCodeSequence>,
  ) { }

  public async getNewCode(): Promise<string> {
    let codeObject = null;
    try {
      codeObject = await this.graphCodeModel.findOne();
    } finally {
      if (!codeObject) {
        codeObject = { code: '' };
      }
    }
    try {
      if (!codeObject.id) {
        codeObject = { code: this.increment('') };
        codeObject = await this.graphCodeModel.create(codeObject);
      } else {
        codeObject.code = this.increment(codeObject.code);
        await this.graphCodeModel.updateOne(codeObject);
      }
    } catch (e) {
      throw new HttpException('Error has been occured while syncing Graph Code Sequence', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return codeObject.code;
  }

  private increment(c) {
    const result = this.next(c).toLowerCase();
    if (result === 'new') {
      return this.next(result).toLowerCase();
    } else {
      return result;
    }
  }

  private next(c) {
    const u = c.toUpperCase();
    if (this.same(u, 'Z')) {
      let txt = '';
      let i = u.length;
      while (i--) {
        txt += '2';
      }
      return (txt + '2');
    } else {
      let p = '';
      let q = '';
      if (u.length > 1) {
        p = u.substring(0, u.length - 1);
        q = String.fromCharCode(p.slice(-1).charCodeAt(0));
      }
      const l = u.slice(-1).charCodeAt(0);
      const z = this.nextLetter(l);
      if (z === '2') {
        return p.slice(0, -1) + this.nextLetter(q.slice(-1).charCodeAt(0)) + z;
      } else {
        return p + z;
      }
    }
  }

  private nextLetter(l) {
    if (l === 57) {// take letters after '9'
      return 'A';
    } else if (l === 78) {// miss 'o'
      return 'P';
    } else if (l === 75) {// miss 'l'
      return 'M';
    } else if (l < 90) {
      return String.fromCharCode(l + 1);
    } else {
      return '2'; // start from 2 (miss '1' and '0')
    }
  }

  private same(str, char) {
    let i = str.length;
    while (i--) {
      if (str[i] !== char) {
        return false;
      }
    }
    return true;
  }
}
