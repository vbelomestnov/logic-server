import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { UserSchema } from '../mongoose/user.schema';
import { getModelToken } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { UserDto } from '../dto/user/user.dto';

const User = mongoose.model('User', UserSchema);

describe('UserService', () => {
  let service: UserService;
  let userModel: Model<UserDto>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getModelToken('User'),
          useValue: User,
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
    userModel = module.get<Model<UserDto>>(getModelToken('User'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('create user', async () => {
    const errorMessage = `Test Error`;
    const spyUserCreateReject = jest.spyOn(userModel, 'create').mockImplementation(() => Promise.reject(new Error(errorMessage)));
    try {
      await service.create('admin', 'password_hash');
      fail('Unexpected behavior');
    } catch (e) {
      expect(spyUserCreateReject).toHaveBeenCalled();
      expect(e.message).toBe(`Create new user error: ${errorMessage}`);
    }
    spyUserCreateReject.mockClear();

    const spyUserCreate = jest.spyOn(userModel, 'create').mockImplementation(() => Promise.resolve({ login: 'admin', password: 'password_hash' }));
    await service.create('admin', 'password_hash');
    expect(spyUserCreate).toHaveBeenCalled();
  });

  it('findOne user', async () => {
    const errorMessage = `Test Error`;
    const spyUserCreateReject = jest.spyOn(userModel, 'findOne')
      .mockImplementation(() => ({ lean: () => Promise.reject(new Error(errorMessage)) }));
    try {
      await service.findOne('admin');
      fail('Unexpected behavior');
    } catch (e) {
      expect(spyUserCreateReject).toHaveBeenCalled();
      expect(e.message).toBe(`Find user error: ${errorMessage}`);
    }
    spyUserCreateReject.mockClear();

    const spyUserCreate = jest.spyOn(userModel, 'findOne')
      .mockImplementation(() => ({ lean: () => Promise.resolve({ login: 'admin', password: 'password_hash' }) }));
    await service.findOne('admin');
    expect(spyUserCreate).toHaveBeenCalled();
  });
});
