import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  login: { type: String, index: { unique: true } },
  password: String,
});
