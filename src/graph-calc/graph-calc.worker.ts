import { parentPort, workerData } from 'worker_threads';
import { CalcTask } from './types/calc-task.interface';
import { GraphCalc } from './graph-calc';
import { Observable, Subscriber, interval } from 'rxjs';
import { timeout, startWith, debounce } from 'rxjs/operators';
import { WorkerMessage } from './types/worker-message.interface';

const observable = new Observable((subscriber: Subscriber<any>) => {
  parentPort.on('message', (value) => {
    console.log('New message');
    subscriber.next({
      task: value.task,
      data: value.data,
    });
  });
});

const { graph } = workerData;
const calc = new GraphCalc(graph);
let index = 0;
observable.pipe(
  // debounce(() => interval(10)),
  startWith({ task: workerData.task, data: workerData.data }),
  // timeout(5000),
  timeout(50),
).subscribe((value) => {
  try {
    const { task, data } = value;
    switch (task) {
      case (CalcTask.addShape):
        calc.addShape(data);
        break;
      case (CalcTask.removeShape):
        calc.removeShape(data);
        break;
      case (CalcTask.addLink):
        calc.addLink(data);
        break;
      case (CalcTask.removeLink):
        const { source, dest } = data;
        calc.removeLink(source, dest);
        break;
      case (CalcTask.setShapePlacements):
        calc.setShapePlacements(data);
        break;
      case (CalcTask.saveTitle):
        calc.saveTitle(data);
        break;
      case (CalcTask.saveShapeText):
        const { shapeName, text } = data;
        calc.saveShapeText(shapeName, text);
        break;
    }
  } catch (e) {
    parentPort.postMessage({ type: WorkerMessage.graph, data: { success: false, graph: calc.graph } });
    parentPort.postMessage({ type: WorkerMessage.outOfBoard, data: calc.isOutOfBoard });
    parentPort.postMessage({ type: WorkerMessage.exception, data: e.message });
  }
  index++;
  console.log(`Task #${index} finished`);
}, (error) => {
  console.log(error);
  if (error.name === 'TimeoutError') {
    parentPort.postMessage({ type: WorkerMessage.graph, data: { success: true, graph: calc.graph } });
  } else {
    parentPort.postMessage({ type: WorkerMessage.graph, data: { success: false, graph: calc.graph } });
    parentPort.postMessage({ type: WorkerMessage.exception, data: error });
  }
});
