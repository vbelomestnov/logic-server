import { Injectable, Inject } from '@nestjs/common';
import { ClientData } from './types/client-data.interface';
import { Socket } from 'socket.io';
import { GraphDto } from '../dto/graph/graph.dto';
import { CalcTask } from './types/calc-task.interface';
import { Rectangle } from '../dto/shapes/rectangle';
import { Link } from '../dto/shapes/link';
import { RemoveLinkParams } from './types/remove-link-params.interface';
import { Worker } from 'worker_threads';
import { resolve } from 'path';
import { WorkerMessage } from './types/worker-message.interface';
import { SaveShapeTextParams } from './types/save-shape-text-params';

@Injectable()
export class GraphCalcClientDataService {
  data: { [key: string]: ClientData } = {};
  Worker = null;

  constructor(
    @Inject('WORKER') WorkerContainer,
  ) {
    this.Worker = WorkerContainer;
  }

  addClient(socket: Socket) {
    console.log(`${socket.id} CONNECTED.`);
    this.data[socket.id] = { socket };
  }

  removeClient(socket: Socket) {
    console.log(`${socket.id} DISCONNECTED.`);
    delete this.data[socket.id];
  }

  initGraph(socket: Socket, graph: GraphDto) {
    if (this.data[socket.id]) {
      this.data[socket.id].graph = graph;
    }
  }

  getGraph(socket: Socket): GraphDto {
    if (!this.data[socket.id] || !this.data[socket.id].graph) {
      throw new Error('Graph hasn\'t been initialized yet');
    }
    return this.data[socket.id].graph;
  }

  addTask(task: CalcTask, taskData: Rectangle | Link | string | RemoveLinkParams | SaveShapeTextParams, socket: Socket): void {
    if (!this.data[socket.id] || !this.data[socket.id].graph) {
      throw new Error('Graph hasn\'t been initialized yet');
    }
    const graph = this.data[socket.id].graph;
    const socketData: ClientData = this.data[socket.id];
    if (socketData.worker) {
      const worker: Worker = this.data[socket.id].worker;
      worker.postMessage({
        task,
        data: taskData,
      });
    } else {
      const worker = new this.Worker(resolve(__dirname, './graph-calc.worker.js'), {
        workerData: {
          graph,
          task,
          data: taskData,
        },
      });
      this.data[socket.id].worker = worker;
      socket.emit('state', 'in_progress');
      worker.on('message', (message) => {
        switch (message.type) {
          case WorkerMessage.graph:
            console.log(`Worker new graph`);
            socket.emit('graph', message.data);
            socketData.graph = message.data.graph;
            break;
          case WorkerMessage.outOfBoard:
            console.log(`Worker out of board`);
            socket.emit('outOfBoard', message.data);
            break;
          case WorkerMessage.exception:
            console.log(`Worker error: ${message.data}`);
            socket.emit('exception', {
              status: 'error',
              message: message.data,
            });
            break;
        }
        console.log('Worker terminate');
        worker.terminate();
        socketData.workerTermination = true;
      });
      worker.on('error', (message) => {
        console.log(`Worker error: ${message}`);
        socket.emit('exception', {
          status: 'error',
          message,
        });
      });
      worker.on('exit', (code) => {
        console.log(`Worker finish`);
        if (code !== 0 && !socketData.workerTermination) {
          socket.emit('exception', {
            status: 'error',
            message: `Worker stopped with exit code ${code}`,
          });
        }
        socket.emit('state', 'pending');
        socketData.worker = null;
        socketData.workerTermination = false;
      });
    }
  }

  breakCalculations(socket: Socket): void {
    if (!this.data[socket.id] || !this.data[socket.id].graph) {
      throw new Error('Graph hasn\'t been initialized yet');
    }
    const socketData: ClientData = this.data[socket.id];
    if (socketData.worker) {
      console.log(`Force worker termination`);
      const worker: Worker = this.data[socket.id].worker;
      worker.terminate();
      socketData.workerTermination = true;
      socket.emit('graph', {success: false, graph: this.data[socket.id].graph});
    }
  }
}
