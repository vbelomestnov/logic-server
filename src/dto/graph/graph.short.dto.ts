import { IsString, Length, IsAlphanumeric } from 'class-validator';

export class GraphShortDto {
  @IsString()
  @Length(1)
  @IsAlphanumeric()
  code: string;

  @IsString()
  @Length(10)
  title: string;
}

export const GRAPH_LIST_STUB: GraphShortDto[] = [
  { code: '0aasffkcjek43kd', title: 'Test graph 1 asdfkekc kfkfd' },
  { code: '0aa', title: 'Test graph 2 asdfkekc kfkfd' },
  { code: '0', title: 'Test graph 3 asdfkekc ' },
  { code: '0aasffkcjek43k', title: 'Test graph 4 asdfkekc ' },
  { code: '0aasffkcje', title: 'Test graph 5 kfkfd' },
  { code: '0', title: 'Test graph 6' },
  { code: '0aa', title: 'Test graph 7' },
  { code: '0a', title: 'Test graph 8 asdfkekc kfkfd' },
  { code: '0aasffkcjek43kd', title: 'Test graph 9 asdfkekc' },
  { code: '0aasffkcjek4', title: 'Test graph 10 asdfkekc asdf asdfa sdf kfkfd' },
  { code: '0aas', title: 'Test graph 11' },
  { code: '0aasffkcjd', title: 'Test graph 12 kfkfd' },
  { code: '0aasffkcjek43kd', title: 'Test graph 13 kfkfd' },
];
