import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import * as mongoose from 'mongoose';
import { UserSchema } from '../mongoose/user.schema';
import { UserService } from '../user/user.service';
import { getModelToken } from '@nestjs/mongoose';
import { hash, genSalt } from 'bcrypt';

const User = mongoose.model('User', UserSchema);

describe('AuthService', () => {
  let service: AuthService;
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: 'test_secret',
          signOptions: { expiresIn: '60s' },
        }),
      ],
      providers: [
        AuthService,
        UserService,
        {
          provide: getModelToken('User'),
          useValue: User,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    userService = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should validate user', async () => {
    const password = 'pass';
    const salt = await genSalt(10);
    const passwordHash = await hash(password, salt);
    const spy = jest.spyOn(userService, 'findOne').mockImplementation(() => Promise.resolve({ login: 'test', password: passwordHash }));
    expect(await service.validateUser('test', password)).toEqual({ login: 'test' });
    expect(await service.validateUser('test', 'false_pass')).toBeFalsy();
  });
});
