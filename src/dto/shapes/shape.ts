import { Point } from './point';
import { ShapeType } from '../enums/shape-type';
import { IsString, Length, IsEnum } from 'class-validator';

export class Shape extends Point {
  @IsString()
  @Length(1)
  name: string;
  @IsEnum(ShapeType)
  type: ShapeType;
  @IsString()
  @Length(10)
  text: string;
}
