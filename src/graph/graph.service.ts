import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { GraphDto } from '../dto/graph/graph.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GraphShortDto } from '../dto/graph/graph.short.dto';
import { GraphListQueryDto } from '../dto/graph/graph-list.query.dto';
import { Rectangle } from '../dto/shapes/rectangle';
import { ShapeType } from '../dto/enums/shape-type';
import { GraphCodeSequenceService } from './graph-code-sequence.service';
import { GraphCalcClientDataService } from '../graph-calc/graph-calc-client-data.service';

import { constants, promises as fs } from 'fs';
import { DOMParser, XMLSerializer } from 'xmldom';
import * as canvas from 'canvas';
import fetch from 'node-fetch';
import Canvg, { presets } from 'canvg';
import { GraphMakingLogService } from './graph-making-log.service';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

const preset = presets.node({
  DOMParser,
  canvas,
  fetch,
});

const SHORT_TIME_LIMIT = 7;
const SHORT_TIME_SPAN = 15 * 60 * 1000; //15 minutes
const LONG_TIME_LIMIT = 30;
const LONG_TIME_SPAN = 24 * 3600 * 1000; //24 hours

@Injectable()
export class GraphService {

  static defaultHeight = 600;
  static defaultWidth = 400;

  constructor(
    @InjectModel('Graph') private readonly graphModel: Model<GraphDto>,
    private graphCodeSequence: GraphCodeSequenceService,
    private graphMakingLog: GraphMakingLogService,
    private graphCalcClientData: GraphCalcClientDataService,
  ) { }

  async saveGraph(socketId: string, svg: string, ipAddress: string): Promise<GraphDto> {
    //Refuse with "BAD_REQUEST" if there isn't client in "graphCalcClientData" service
    if (!this.graphCalcClientData.data[socketId] || !this.graphCalcClientData.data[socketId].graph) {
      throw new HttpException('There isn\'t initialized graph for client connection', HttpStatus.BAD_REQUEST);
    }

    //Refuse if it was too many requests (SHORT_TIME_LIMIT) in last SHORT_TIME_SPAN
    const smallTimeLimitCount = await this.graphMakingLog.checkTimeLimitCount(ipAddress, SHORT_TIME_SPAN);
    if (smallTimeLimitCount >= SHORT_TIME_LIMIT) {
      throw new HttpException('Limit of new graphs has been used up. Try later', HttpStatus.TOO_MANY_REQUESTS);
    }

    //Refuse if it was too many requests (LONG_TIME_LIMIT) in last LONG_TIME_SPAN
    const bigTimeLimitCount = await this.graphMakingLog.checkTimeLimitCount(ipAddress, LONG_TIME_SPAN);
    if (bigTimeLimitCount >= LONG_TIME_LIMIT) {
      throw new HttpException('Limit of new graphs has been used up. Try later', HttpStatus.TOO_MANY_REQUESTS);
    }

    const code = await this.graphCodeSequence.getNewCode();
    const graph = this.graphCalcClientData.data[socketId].graph;

    const errors = await validate(plainToClass(GraphDto,
      {
        ...graph,
        code,
      },
    ));
    if (errors.length) {
      throw new HttpException({ message: errors }, HttpStatus.BAD_REQUEST);
    }

    await this.saveSvg(svg, graph.width, graph.height, code);
    try {
      await this.graphMakingLog.log(code, ipAddress);
      const newGraph = await this.graphModel.create({
        ...graph,
        code,
        approved: false,
        archived: false,
      });
      return newGraph;
    } catch (e) {
      console.error(e);
      throw new HttpException('DB error has been occured while creating new Graph', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getGraph(code: string): Promise<GraphDto> {
    let graph = null;
    try {
      if (code === 'new') {
        graph = {
          width: GraphService.defaultWidth,
          height: GraphService.defaultHeight,
          code: '',
          title: '',
          shapes: [],
          links: [],
        };
      } else {
        graph = await this.graphModel.findOne({ code }).lean();
        if (graph) {
          delete graph._id;
          delete graph.__v;
          for (const shape of graph.shapes) {
            delete shape._id;
          }
          for (const link of graph.links) {
            delete link._id;
          }
        }
      }
    } catch (e) {
      console.error(e);
      throw new HttpException('DB error has been occured while creating new Graph', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    if (!graph) {
      console.error(`Graph not found ${code}`);
      throw new HttpException(`Graph not found ${code}`, HttpStatus.BAD_REQUEST);
    }
    return graph;
  }

  async getGraphList(query: GraphListQueryDto): Promise<GraphShortDto[]> {
    try {
      const filter = {
        ...(query.search ? { $text: { $search: query.search } } : {}),
        archived: false,
      };
      return await this.graphModel.find(filter, 'title code', { skip: query.skip, limit: 20 }).sort({ approved: 'desc' });
    } catch (e) {
      console.error(e);
      throw new HttpException('DB error has been occured while getting list of Graphs', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async reviewGraph(code: string, approved: boolean): Promise<boolean> {
    let result: { n?: boolean } = {};
    try {
      result = await this.graphModel.updateOne({ code }, { approved });
    } catch (e) {
      console.error(e);
      throw new HttpException('DB error has been occured while reviewing a Graph', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    if (!result.n) {
      console.error(`Graph not found ${code}`);
      throw new HttpException(`Graph not found ${code}`, HttpStatus.BAD_REQUEST);
    }
    return !!result.n;
  }

  async archiveGraph(code: string): Promise<boolean> {
    let result: { n?: boolean } = {};
    try {
      result = await this.graphModel.updateOne({ code }, { archived: true });
    } catch (e) {
      console.error(e);
      throw new HttpException('DB error has been occured while archiving a Graph', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    if (!result.n) {
      console.error(`Graph not found ${code}`);
      throw new HttpException(`Graph not found ${code}`, HttpStatus.BAD_REQUEST);
    }
    return !!result.n;
  }

  private async saveSvg(svg: string, width: number, height: number, code: string) {
    const doc = (new DOMParser()).parseFromString(svg);
    const def = (new DOMParser()).parseFromString(
      `<defs>
        <filter id="filterBlur">
          <feGaussianBlur stdDeviation="0.9" />
        </filter>
      </defs>`,
    );
    const text = (new DOMParser()).parseFromString(
      `<g transform="translate(13 30)">
        <text x="0" y="0" font-size="24" font-weight="bold" fill="#1a237e"
          filter="url(#filterBlur)">
          impacton.world/graph/${code}
        </text>
        <text transform="translate(1 1)" x="0" y="0" font-weight="bold" font-size="24" stroke="white" stroke-width="0.8"
          fill="#1a237e">
          impacton.world/graph/${code}
        </text>
      </g>`,
    );
    doc.firstChild.insertBefore(def.firstChild, doc.firstChild.firstChild);
    doc.firstChild.appendChild(text.firstChild);
    const cnv = preset.createCanvas(width, height);
    const ctx = cnv.getContext('2d');
    const v = Canvg.fromString(ctx, (new XMLSerializer()).serializeToString(doc), preset);
    v.resize(2040, 912, 'xMidYMid meet');
    await v.render();
    const png = cnv.toBuffer();
    try {
      await fs.access(`${__dirname}/../../../uploads`, constants.R_OK);
    } catch (e) {
      await fs.mkdir(`${__dirname}/../../../uploads`);
    }
    try {
      await fs.access(`${__dirname}/../../../uploads/test`, constants.R_OK);
    } catch (e) {
      await fs.mkdir(`${__dirname}/../../../uploads/test`);
    }
    try {
      await fs.access(`${__dirname}/../../../uploads/test/png`, constants.R_OK);
    } catch (e) {
      await fs.mkdir(`${__dirname}/../../../uploads/test/png`);
    }
    await fs.writeFile(`${__dirname}/../../../uploads/test/png/${code}.png`, png);
  }
}

const RECT1: Rectangle = {
  name: 'object-100',
  type: ShapeType.rectangle,
  x: 50,
  y: 50,
  width: 200,
  height: 100,
  text: 'RECT1 - asdf asdf asdfasd asd fasd fasd fasd fasdf asdf asdf asdf asdf asdf asdf asdf asdf asdffa asdf asdf asdf...',
};

const RECT2: Rectangle = {
  name: 'object-101',
  type: ShapeType.rectangle,
  x: 93,
  y: 487,
  width: 200,
  height: 100,
  text: 'RECT2 - asdf asdf asdfasd asd fasd fasd fasd fasdf asdf asdf asdf asdf asdf asdf asdf asdf asdffa asdf asdf asdf...',
};

const RECT3: Rectangle = {
  name: 'object-102',
  type: ShapeType.rectangle,
  x: 40,
  y: 232,
  width: 178,
  height: 50,
  text: 'RECT3 - qwew qeqw erqwer qwe rqwerweq rqwr eqwerq...',
};

const RECT4: Rectangle = {
  name: 'object-103',
  type: ShapeType.rectangle,
  x: 42,
  y: 305,
  width: 56,
  height: 50,
  text: 'RECT4 - t rt r tr tr rt rt r tr tr rt rtrt rt...',
};

const RECT5: Rectangle = {
  name: 'object104',
  type: ShapeType.rectangle,
  x: 164,
  y: 384,
  width: 100,
  height: 50,
  text: 'RECT5 - nbmn bnmb b bn mb bn bnmbn mbn bnm bn mbnm bnm bnm...',
};

export const graphStubData = (): GraphDto => {
  return {
    code: '2a',
    title: 'Test eate dn graph',
    width: 400,
    height: 600,
    shapes: [
      { ...RECT1 },
      { ...RECT2 },
      { ...RECT3 },
      { ...RECT4 },
      { ...RECT5 },
    ],
    links: [
      {
        source: RECT2.name,
        dest: RECT1.name,
        translation: null,
        rotation: 0,
        length: 0,
        route: null,
      },
      {
        source: RECT4.name,
        dest: RECT1.name,
        translation: null,
        rotation: 0,
        length: 0,
        route: null,
      },
      {
        source: RECT1.name,
        dest: RECT3.name,
        translation: null,
        rotation: 0,
        length: 0,
        route: null,
      },
    ],
  };
};

export const graphStubData2 = (): GraphDto => {
  return {
    code: '2a2',
    title: 'Test eate dn graph 2',
    width: 400,
    height: 600,
    shapes: [
      { ...RECT1 },
      { ...RECT2 },
    ],
    links: [
      {
        source: RECT2.name,
        dest: RECT1.name,
        translation: null,
        rotation: 0,
        length: 0,
        route: null,
      },
    ],
  };
};
