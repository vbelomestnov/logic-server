import { IsString, Length, IsAlphanumeric } from 'class-validator';

export class GraphParamsDto {
  @IsString()
  @Length(1)
  @IsAlphanumeric()
  code: string;
}
