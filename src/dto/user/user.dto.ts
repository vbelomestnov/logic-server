import { IsString, Length, IsAlphanumeric } from 'class-validator';

export class UserDto {
  @IsString()
  @Length(6)
  @IsAlphanumeric()
  login: string;

  @IsString()
  @Length(8)
  password: string;
}
