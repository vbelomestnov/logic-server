import * as mongoose from 'mongoose';

export const GraphCodeSequenceSchema = new mongoose.Schema({
  code: String,
});
