import { Controller, Get, Param, Post, Body, Query, UsePipes, ValidationPipe, Put, UseGuards, Delete, Req } from '@nestjs/common';
import { GraphDto } from '../dto/graph/graph.dto';
import { GraphService } from './graph.service';
import { GraphShortDto } from '../dto/graph/graph.short.dto';
import { GraphParamsDto } from '../dto/graph/graph.params.dto';
import { GraphListQueryDto } from '../dto/graph/graph-list.query.dto';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

@Controller('graph')
@UsePipes(new ValidationPipe({transform: true}))
export class GraphController {
  constructor(private readonly graphService: GraphService) { }

  @Get()
  async getGraphList(@Query() query: GraphListQueryDto): Promise<GraphShortDto[]> {
    return this.graphService.getGraphList(query);
  }

  @Get(':code')
  async getGraph(@Param() params: GraphParamsDto): Promise<GraphDto> {
    return this.graphService.getGraph(params.code);
  }

  @Post()
  async saveGraph(@Body('socketId') socketId: string, @Body('svg') svg: string, @Req() request: Request): Promise<GraphDto> {
    return this.graphService.saveGraph(socketId, svg, request.ip);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':code')
  async removeGraph(@Param() params: GraphParamsDto): Promise<boolean> {
    return this.graphService.archiveGraph(params.code);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':code/review')
  async reviewGraph(@Body('approved') approved: boolean, @Param() params: GraphParamsDto): Promise<boolean> {
    return this.graphService.reviewGraph(params.code, approved);
  }
}
