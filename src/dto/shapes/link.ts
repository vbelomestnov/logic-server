import { Point } from './point';
import { IsString, Length, IsNumber, IsArray, Min, ValidateNested, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';

export class Link {

    @IsString()
    @Length(5)
    source: string;

    @IsString()
    @Length(5)
    dest: string;

    @IsOptional()
    @ValidateNested()
    @Type(() => Point)
    translation: Point;

    @IsNumber()
    rotation: number;

    @IsNumber()
    @Min(0)
    length: number;

    @IsOptional()
    @IsArray()
    route: Point[][];
}
