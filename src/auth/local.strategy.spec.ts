import { Test, TestingModule } from '@nestjs/testing';
import * as mongoose from 'mongoose';
import { UserSchema } from '../mongoose/user.schema';
import { LocalStrategy } from './local.strategy';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { getModelToken } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';

const User = mongoose.model('User', UserSchema);

describe('LocalStrategy', () => {
  let service: LocalStrategy;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: 'test_secret',
          signOptions: { expiresIn: '60s' },
        }),
      ],
      providers: [
        AuthService,
        UserService,
        {
          provide: getModelToken('User'),
          useValue: User,
        },
        LocalStrategy,
      ],
    }).compile();

    service = module.get<LocalStrategy>(LocalStrategy);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should validate', async () => {
    const spy = jest.spyOn(authService, 'validateUser').mockImplementation(() => Promise.resolve({ _id: 'id', login: 'test' }));
    expect(await service.validate('test', 'pass')).toEqual({ _id: 'id', login: 'test' });
    spy.mockClear();

    const spyNull = jest.spyOn(authService, 'validateUser').mockImplementation(() => Promise.resolve(null));
    try {
      await service.validate('test', 'pass');
      fail('Unexpected behavior');
    } catch (e) {
      expect(spyNull).toHaveBeenCalled();
      expect(e.message).toBeTruthy();
    }
  });
});
