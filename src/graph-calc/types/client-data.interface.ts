import { GraphDto } from 'src/dto/graph/graph.dto';
import { Socket } from 'socket.io';
import { Worker } from 'worker_threads';

export interface ClientData {
  socket: Socket;
  graph?: GraphDto;
  worker?: Worker;
  workerTermination?: boolean;
}
