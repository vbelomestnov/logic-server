import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDto } from '../dto/user/user.dto';
import { hash, genSalt } from 'bcrypt';
import { SALT_ROUNDS } from '../constants';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<UserDto>,
  ) { }

  async create(login: string, password: string): Promise<UserDto> {
    try {
      const salt = await genSalt(SALT_ROUNDS);
      const passwordHash = await hash(password, salt);
      const newUser = await this.userModel.create({
        login,
        password: passwordHash,
      });
      return newUser;
    } catch (e) {
      console.error(e);
      throw new Error(`Create new user error: ${e.message}`);
    }
  }

  async findOne(login: string): Promise<UserDto> {
    try {
      const user = await this.userModel.findOne({ login }).lean();
      return user;
    } catch (e) {
      console.error(e);
      throw new Error(`Find user error: ${e.message}`);
    }
  }
}
