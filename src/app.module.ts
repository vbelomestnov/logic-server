import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphController } from './graph/graph.controller';
import { GraphService } from './graph/graph.service';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphCodeSequenceSchema } from './mongoose/graph-code-sequence.schema';
import { GraphSchema } from './mongoose/graph.schema';
import { GraphCodeSequenceService } from './graph/graph-code-sequence.service';
import { GraphCalcGateway } from './graph-calc/graph-calc.gateway';
import { GraphCalcClientDataService } from './graph-calc/graph-calc-client-data.service';
import { Worker } from 'worker_threads';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { GraphMakingLogSchema } from './mongoose/graph-making-log.schema';
import { GraphMakingLogService } from './graph/graph-making-log.service';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/nest_test'),
    MongooseModule.forFeature([{ name: 'GraphCodeSequence', schema: GraphCodeSequenceSchema }]),
    MongooseModule.forFeature([{ name: 'GraphMakingLog', schema: GraphMakingLogSchema }]),
    MongooseModule.forFeature([{ name: 'Graph', schema: GraphSchema }]),
    UserModule,
    AuthModule,
  ],
  controllers: [AppController, GraphController],
  providers: [
    AppService,
    GraphService,
    GraphCodeSequenceService,
    GraphMakingLogService,
    GraphCalcGateway,
    GraphCalcClientDataService,
    { provide: 'WORKER', useValue: Worker },
  ],
})
export class AppModule { }
