import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { getModelToken } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { UserSchema } from '../mongoose/user.schema';
import { UserService } from '../user/user.service';

const User = mongoose.model('User', UserSchema);

describe('Auth Controller', () => {
  let controller: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      imports: [
        JwtModule.register({
          secret: 'test_secret',
          signOptions: { expiresIn: '60s' },
        }),
      ],
      providers: [
        AuthService,
        UserService,
        {
          provide: getModelToken('User'),
          useValue: User,
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should post login', async () => {
    const request = { user: { login: 'test', _id: mongoose.Types.ObjectId() } };
    expect((await controller.login(request)).hasOwnProperty('access_token')).toBeTruthy();
  });

  it('should get login', async () => {
    const request = { user: { login: 'test', _id: mongoose.Types.ObjectId() } };
    expect((await controller.getUser(request))).toEqual(request.user);
  });
});
