import { IsString, Length } from 'class-validator';

export class SaveShapeTextParams {
  @IsString()
  @Length(5)
  shapeName: string;

  @IsString()
  @Length(10)
  text: string;
}
