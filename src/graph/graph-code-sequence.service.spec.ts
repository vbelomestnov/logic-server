import { Test, TestingModule } from '@nestjs/testing';
import { GraphCodeSequenceService } from './graph-code-sequence.service';
import * as mongoose from 'mongoose';
import { GraphCodeSequenceSchema } from '../mongoose/graph-code-sequence.schema';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GraphCodeSequence } from './types/graph-code-sequence.interface';
import { HttpStatus } from '@nestjs/common';

const GraphCodeSequenceModel = mongoose.model('GraphCodeSequence', GraphCodeSequenceSchema);

describe('GraphCodeSequenceService', () => {
  let service: GraphCodeSequenceService;
  let codeSequenceModel: Model<GraphCodeSequence>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GraphCodeSequenceService,
        {
          provide: getModelToken('GraphCodeSequence'),
          useValue: GraphCodeSequenceModel,
        },
      ],
    }).compile();

    service = module.get<GraphCodeSequenceService>(GraphCodeSequenceService);
    codeSequenceModel = module.get<Model<GraphCodeSequence>>(getModelToken('GraphCodeSequence'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('new code', async () => {
    let currentCodeObject = { id: '123', code: '2a' };
    const spyCodeSequenceFindOne = jest.spyOn(codeSequenceModel, 'findOne').mockImplementation(
      () => Promise.resolve(currentCodeObject),
    );
    const spyCodeSequenceyUpdate = jest.spyOn(codeSequenceModel, 'updateOne').mockImplementation(() => Promise.resolve());
    const spyCodeSequenceyCreate = jest.spyOn(codeSequenceModel, 'create').mockImplementation(
      () => Promise.resolve({
        ...currentCodeObject,
        code: '2',
      }),
    );
    let result = await service.getNewCode();
    expect(spyCodeSequenceFindOne).toHaveBeenCalledTimes(1);
    expect(spyCodeSequenceyUpdate).toHaveBeenCalledTimes(1);
    expect(spyCodeSequenceyCreate).toHaveBeenCalledTimes(0);
    expect(result).toBe('2b');

    currentCodeObject = null;
    result = await service.getNewCode();
    expect(spyCodeSequenceFindOne).toHaveBeenCalledTimes(2);
    expect(spyCodeSequenceyUpdate).toHaveBeenCalledTimes(1);
    expect(spyCodeSequenceyCreate).toHaveBeenCalledTimes(1);
    expect(result).toBe('2');

    spyCodeSequenceyCreate.mockImplementation(() => Promise.reject('Test error'));
    try {
      await service.getNewCode();
    } catch (e) {
      expect(e.message).toBe('Error has been occured while syncing Graph Code Sequence');
      expect(e.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    expect(spyCodeSequenceFindOne).toHaveBeenCalledTimes(3);
    expect(spyCodeSequenceyCreate).toHaveBeenCalledTimes(2);
  });

  it('exclude "new" code', async () => {
    const currentCodeObject = { id: '123', code: 'nev' };
    jest.spyOn(codeSequenceModel, 'findOne').mockImplementation(
      () => Promise.resolve(currentCodeObject),
    );
    jest.spyOn(codeSequenceModel, 'updateOne').mockImplementation(() => Promise.resolve());
    const result = await service.getNewCode();
    expect(result).toBe('nex');
  });

  it('after "z" should be "22", after "zz" should be "222" etc', async () => {
    let currentCodeObject = { id: '123', code: 'z' };
    jest.spyOn(codeSequenceModel, 'findOne').mockImplementation(
      () => Promise.resolve(currentCodeObject),
    );
    jest.spyOn(codeSequenceModel, 'updateOne').mockImplementation(() => Promise.resolve());
    let result = await service.getNewCode();
    expect(result).toBe('22');

    currentCodeObject = { id: '123', code: 'zz' };
    result = await service.getNewCode();
    expect(result).toBe('222');
  });

  it('increment should work correctly if one symbol', async () => {
    const currentCodeObject = { id: '123', code: 'c' };
    jest.spyOn(codeSequenceModel, 'findOne').mockImplementation(
      () => Promise.resolve(currentCodeObject),
    );
    jest.spyOn(codeSequenceModel, 'updateOne').mockImplementation(() => Promise.resolve());
    const result = await service.getNewCode();
    expect(result).toBe('d');
  });

  it('after "2az" should be "2b2"', async () => {
    const currentCodeObject = { id: '123', code: '2az' };
    jest.spyOn(codeSequenceModel, 'findOne').mockImplementation(
      () => Promise.resolve(currentCodeObject),
    );
    jest.spyOn(codeSequenceModel, 'updateOne').mockImplementation(() => Promise.resolve());
    const result = await service.getNewCode();
    expect(result).toBe('2b2');
  });

  it('skip confusing symbols ("1", "o", "l")', async () => {
    let currentCodeObject = { id: '123', code: '2z' };
    jest.spyOn(codeSequenceModel, 'findOne').mockImplementation(
      () => Promise.resolve(currentCodeObject),
    );
    jest.spyOn(codeSequenceModel, 'updateOne').mockImplementation(() => Promise.resolve());
    let result = await service.getNewCode();
    expect(result).toBe('32');

    currentCodeObject = { id: '123', code: '66n' };
    result = await service.getNewCode();
    expect(result).toBe('66p');

    currentCodeObject = { id: '123', code: 'k' };
    result = await service.getNewCode();
    expect(result).toBe('m');
  });

  it('letters after digits', async () => {
    const currentCodeObject = { id: '123', code: '999' };
    jest.spyOn(codeSequenceModel, 'findOne').mockImplementation(
      () => Promise.resolve(currentCodeObject),
    );
    jest.spyOn(codeSequenceModel, 'updateOne').mockImplementation(() => Promise.resolve());
    const result = await service.getNewCode();
    expect(result).toBe('99a');
  });

});
