export class GraphMakingLog {
  code: string;
  ipAddress: string;
  createdDate: Date;
}
