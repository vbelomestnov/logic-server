import { CalcRectangle } from './calc-rectangle.interface';

export type CalcShape = CalcRectangle;
