import {IsInt, IsNumberString, Min, Max} from 'class-validator';
import { Transform } from 'class-transformer';

export class GraphListQueryDto {
  search: string;

  @Transform(value => parseInt(value, 10), { toClassOnly: true })
  @IsInt()
  @Min(0)
  skip: number;
}
