import { Test, TestingModule } from '@nestjs/testing';
import { GraphMakingLogService } from './graph-making-log.service';
import { getModelToken } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { GraphMakingLogSchema } from '../mongoose/graph-making-log.schema';

const GraphMakingLogModel = mongoose.model('GraphMakingLog', GraphMakingLogSchema);

describe('GraphMakingLogService', () => {
  let service: GraphMakingLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GraphMakingLogService,
        {
          provide: getModelToken('GraphMakingLog'),
          useValue: GraphMakingLogModel,
        },
      ],
    }).compile();

    service = module.get<GraphMakingLogService>(GraphMakingLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
