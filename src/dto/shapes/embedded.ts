import { IsString, Length, IsAlphanumeric } from 'class-validator';
import { Rectangle } from './rectangle';

export class Embedded extends Rectangle {
  @IsString()
  @Length(1)
  @IsAlphanumeric()
  code: string;
}
