import { Test, TestingModule } from '@nestjs/testing';
import { SeederService } from './seeder.service';
import { Logger } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { UserSchema } from '../../mongoose/user.schema';
import { UserService } from '../../user/user.service';

const User = mongoose.model('User', UserSchema);

describe('SeederService', () => {
  let service: SeederService;
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        SeederService,
        Logger,
        {
          provide: getModelToken('User'),
          useValue: User,
        },
      ],
    }).compile();

    userService = module.get<UserService>(UserService);
    service = module.get<SeederService>(SeederService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should seed data', () => {
    const spyCreate = jest.spyOn(userService, 'create').mockImplementation(() => Promise.resolve({ login: 'admin', password: 'password_hash' }));
    service.seed();
    expect(spyCreate).toHaveBeenCalled();
  });
});
