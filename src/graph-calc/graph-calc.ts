import { GraphDto } from '../dto/graph/graph.dto';
import { Shape } from '../dto/shapes/shape';
import { ShapeType } from '../dto/enums/shape-type';
import { CalcRectangle } from './types/calc-rectangle.interface';
import { LineType } from './types/line-type.interface';
import { Side } from './types/side.interface';
import { Point } from '../dto/shapes/point';
import { DimensionLine } from './types/dimension-line.interface';
import { CalcShape } from './types/calc-shape.interface';
import { Link } from '../dto/shapes/link';
import { Rectangle } from 'src/dto/shapes/rectangle';

export class GraphCalc {

  readonly defaultSpan = 10;

  private isMovingShapeOutOfBoard = false;
  get isOutOfBoard(): boolean {
    return this.isMovingShapeOutOfBoard;
  }
  get hasIntersection(): boolean {
    let result = this.isMovingShapeOutOfBoard;
    if (!result) {
      for (const shape of this.graph.shapes) {
        const calcShape = shape as CalcShape;
        if (calcShape.isCovered) {
          result = true;
          break;
        }
      }
    }
    return result;
  }

  constructor(public graph: GraphDto) {
    this.initPlacements();
  }

  addShape(newShape: Shape): GraphDto {
    this.graph.shapes.push(newShape as CalcRectangle);
    this.doSetShapePlacements(newShape);
    if (this.hasIntersection) {
      this.graph.shapes = this.graph.shapes.filter((shape: Shape) => shape.name !== newShape.name);
      newShape.x = null;
      newShape.y = null;
      this.initPlacements();
      throw new Error('New shape overlaps old ones');
    }
    this.initPlacements();
    return this.graph;
  }

  removeShape(shapeName: string): GraphDto {
    for (const link of this.graph.links) {
      if (link.source === shapeName || link.dest === shapeName) {
        this.removeLink(link.source, link.dest);
      }
    }
    this.graph.shapes = this.graph.shapes.filter((shape: Shape) => shape.name !== shapeName);
    this.initPlacements();
    return this.graph;
  }

  addLink(newLink: Link): GraphDto {
    if (newLink.source === newLink.dest) {
      throw new Error('Source and destination are match');
    }
    let hasDuplicate = false;
    for (const link of this.graph.links) {
      if (newLink.source === link.source && newLink.dest === link.dest) {
        hasDuplicate = true;
        break;
      }
    }
    if (hasDuplicate) {
      throw new Error('There is duplicate link');
    }
    this.graph.links.push(newLink);
    this.buildLink(newLink);
    return this.graph;
  }

  removeLink(source: string, dest: string): GraphDto {
    this.graph.links = this.graph.links.filter((link: Link) => !(link.source === source && link.dest === dest));
    return this.graph;
  }

  setShapePlacements(shape: Shape): GraphDto {
    const originShapeIndex = this.graph.shapes.findIndex((aShape: Shape) => aShape.name === shape.name);
    const originShape = this.graph.shapes.splice(originShapeIndex, 1)[0];
    this.graph.shapes.push(shape as Rectangle);
    this.doSetShapePlacements(shape);
    if (this.hasIntersection) {
      this.replaceShape(originShape);
      throw new Error('Shape overlaps other ones');
    }
    this.initPlacements();
    return this.graph;
  }

  saveTitle(title: string): GraphDto {
    this.graph.title = title;
    return this.graph;
  }

  saveShapeText(shapeName: string, text: string): GraphDto {
    const shape = this.graph.shapes.find((aShape: Shape) => aShape.name === shapeName);
    shape.text = text;
    return this.graph;
  }

  private initPlacements(): void {
    for (const shape of this.graph.shapes) {
      this.initShapePlacements(shape);
    }

    for (const link of this.graph.links) {
      this.buildLink(link);
    }
  }

  private doSetShapePlacements(shape: Shape): void {
    this.initShapePlacements(shape);
    this.checkForOutOfBoard(shape as CalcShape);
    this.checkForIntersections(shape);
  }

  private initShapePlacements(shape: Shape): void {
    const ds = this.defaultSpan;
    switch (shape.type) {
      case ShapeType.rectangle:
        const rect: CalcRectangle = shape as CalcRectangle;
        rect.placement = [
          {
            type: LineType.horizontal, side: Side.top,
            points: [
              { x: rect.x - ds, y: rect.y - ds },
              { x: rect.x + rect.width + ds, y: rect.y - ds },
              { x: rect.x + Math.trunc(rect.width / 2), y: rect.y },
              { x: rect.x + Math.trunc(rect.width / 2), y: rect.y - ds - 1 },
            ],
          },
          {
            type: LineType.vertical, side: Side.right,
            points: [
              { x: rect.x + rect.width + ds, y: rect.y - ds },
              { x: rect.x + rect.width + ds, y: rect.y + rect.height + ds },
              { x: rect.x + rect.width, y: rect.y + Math.trunc(rect.height / 2) },
              { x: rect.x + rect.width + ds + 1, y: rect.y + Math.trunc(rect.height / 2) },
            ],
          },
          {
            type: LineType.horizontal, side: Side.bottom,
            points: [
              { x: rect.x - ds, y: rect.y + rect.height + ds },
              { x: rect.x + rect.width + ds, y: rect.y + rect.height + ds },
              { x: rect.x + Math.trunc(rect.width / 2), y: rect.y + rect.height },
              { x: rect.x + Math.trunc(rect.width / 2), y: rect.y + rect.height + ds + 1 },
            ],
          },
          {
            type: LineType.vertical, side: Side.left,
            points: [
              { x: rect.x - ds, y: rect.y - ds },
              { x: rect.x - ds, y: rect.y + rect.height + ds },
              { x: rect.x, y: rect.y + Math.trunc(rect.height / 2) },
              { x: rect.x - ds - 1, y: rect.y + Math.trunc(rect.height / 2) },
            ],
          },
        ];
        rect.selfPlacement = [
          {
            type: LineType.horizontal, side: Side.top,
            points: [
              { x: rect.x, y: rect.y },
              { x: rect.x + rect.width, y: rect.y },
            ],
          },
          {
            type: LineType.vertical, side: Side.right,
            points: [
              { x: rect.x + rect.width, y: rect.y },
              { x: rect.x + rect.width, y: rect.y + rect.height },
            ],
          },
          {
            type: LineType.horizontal, side: Side.bottom,
            points: [
              { x: rect.x, y: rect.y + rect.height },
              { x: rect.x + rect.width, y: rect.y + rect.height },
            ],
          },
          {
            type: LineType.vertical, side: Side.left,
            points: [
              { x: rect.x, y: rect.y },
              { x: rect.x, y: rect.y + rect.height },
            ],
          },
        ];
        break;
    }
  }

  private checkForOutOfBoard(shape: CalcShape): void {
    const points: Point[] = shape.placement
      .map((line: DimensionLine) => line.points)
      .reduce((accum: Point[], aPoints: Point[]) => (accum = accum.concat(aPoints)));
    this.isMovingShapeOutOfBoard = false;
    for (const point of points) {
      if (point.x < 0 || point.y < 0 || point.x > this.graph.width || point.y > this.graph.height) {
        this.isMovingShapeOutOfBoard = true;
        break;
      }
    }
  }

  private checkForIntersections(shape: Shape): void {
    this.resetOverlapStates();
    const otherShapes: Shape[] = this.graph.shapes.filter((aShape: Shape) => aShape.name !== shape.name);
    for (const anotherShape of otherShapes) {
      if (this.isShapesIntersect(shape as CalcShape, anotherShape as CalcShape)
        || this.isShapesIntersect(anotherShape as CalcShape, shape as CalcShape)
        || !this.isNotShapeInsideAnother(shape as CalcShape, anotherShape as CalcShape)
        || !this.isNotShapeInsideAnother(anotherShape as CalcShape, shape as CalcShape)) {
        (anotherShape as CalcShape).isCovered = true;
      }
    }
    this.checkForShortDimensions(shape);
  }

  private resetOverlapStates() {
    for (const shape of this.graph.shapes) {
      const calcShape = shape as CalcShape;
      calcShape.isCovered = false;
    }
  }

  private isShapesIntersect(shape1: CalcShape, shape2: CalcShape): boolean {
    const hLines: Point[][] = shape1.placement
      .filter((line: DimensionLine) => line.type === LineType.horizontal)
      .map((line: DimensionLine) => line.points);
    const vLines = shape2.placement
      .filter((line: DimensionLine) => line.type === LineType.vertical)
      .map((line: DimensionLine) => line.points);
    let intersect = false;
    for (const hLine of hLines) {
      for (const vLine of vLines) {
        if (
          hLine[0].x <= vLine[0].x && hLine[1].x >= vLine[0].x && vLine[0].y <= hLine[0].y && vLine[1].y >= hLine[0].y
        ) {
          intersect = true;
          break;
        }
      }
      if (intersect) {
        break;
      }
    }
    return intersect;
  }

  private isNotShapeInsideAnother(shape1: CalcShape, shape2: CalcShape) {
    const vLine1: Point[] = shape1.placement.find((line: DimensionLine) => line.type === LineType.vertical).points;
    const vLine2: Point[] = shape2.placement.find((line: DimensionLine) => line.type === LineType.vertical).points;
    const hLine2: Point[] = shape2.placement.find((line: DimensionLine) => line.type === LineType.horizontal).points;
    return !(vLine1[0].y > vLine2[0].y && vLine1[1].y < vLine2[1].y && vLine1[0].x > hLine2[0].x && vLine1[0].x < vLine2[1].x);
  }

  private checkForShortDimensions(shape: Shape): void {
    const rect: CalcRectangle = shape as CalcRectangle;
    if (rect.width < 20 || rect.height < 20) {
      rect.isCovered = true;
    }
  }

  private buildLink(link: Link): Link {
    const sourceShape = this.graph.shapes.find((shape: Shape) => shape.name === link.source) as CalcShape;
    const destShape = this.graph.shapes.find((shape: Shape) => shape.name === link.dest) as CalcShape;

    const sourceSides = [
      sourceShape.placement.find((line: DimensionLine) => line.side === Side.top),
      sourceShape.placement.find((line: DimensionLine) => line.side === Side.bottom),
      sourceShape.placement.find((line: DimensionLine) => line.side === Side.left),
      sourceShape.placement.find((line: DimensionLine) => line.side === Side.right),
    ];
    const destSides = [
      destShape.placement.find((line: DimensionLine) => line.side === Side.bottom),
      destShape.placement.find((line: DimensionLine) => line.side === Side.top),
      destShape.placement.find((line: DimensionLine) => line.side === Side.right),
      destShape.placement.find((line: DimensionLine) => line.side === Side.left),
    ];
    let route: Point[][] = null;
    let minDist = 0;
    for (const source of sourceSides) {
      for (const dest of destSides) {
        const sourceMiddlePoint = source.points[2];
        const destMiddlePoint = dest.points[2];
        const intersectRoute = [sourceMiddlePoint, destMiddlePoint];
        if (!this.isLinePlacementIntersect(intersectRoute, sourceShape.selfPlacement)
          && !this.isLinePlacementIntersect(intersectRoute, destShape.selfPlacement)) {
          const middleShapes = this.graph.shapes.filter((shape: Shape) => shape.name !== link.source && shape.name !== link.dest);
          const minRoute = this.getMinRouteForSegment([sourceMiddlePoint, destMiddlePoint], middleShapes);
          const curDist = this.routeDistance(minRoute);
          if ((!minDist || curDist < minDist) && !this.isLinePlacementIntersect(minRoute[0], sourceShape.selfPlacement)) {
            minDist = curDist;
            route = minRoute;
          }
        }
      }
    }

    const l = route.length;
    link.translation = { x: route[l - 1][0].x, y: route[l - 1][0].y };
    link.rotation = Math.atan2((route[l - 1][1].y - route[l - 1][0].y), (route[l - 1][1].x - route[l - 1][0].x)) * 180 / Math.PI;
    link.length = this.distance(route[l - 1][0], route[l - 1][1]);
    link.route = route.slice(0, route.length - 1);

    return link;
  }

  private isLinePlacementIntersect(line: Point[], placement: DimensionLine[]): boolean {
    const lines = placement.map((aLine: DimensionLine) => aLine.points);
    let isIntersect = false;
    for (const pLine of lines) {
      const intersection = this.checkLineIntersection(line, pLine);
      if (intersection.onLine1 && intersection.onLine2) {
        isIntersect = true;
        break;
      }
    }
    return isIntersect;
  }

  private checkLineIntersection(line1: Point[], line2: Point[]) {
    const result = {
      x: null,
      y: null,
      onLine1: false,
      onLine2: false,
    };
    const denominator = ((line2[1].y - line2[0].y) * (line1[1].x - line1[0].x)) - ((line2[1].x - line2[0].x) * (line1[1].y - line1[0].y));
    if (denominator === 0) {
      return result;
    }
    let a = line1[0].y - line2[0].y;
    let b = line1[0].x - line2[0].x;
    const numerator1 = ((line2[1].x - line2[0].x) * a) - ((line2[1].y - line2[0].y) * b);
    const numerator2 = ((line1[1].x - line1[0].x) * a) - ((line1[1].y - line1[0].y) * b);
    a = numerator1 / denominator;
    b = numerator2 / denominator;

    // if we cast these lines infinitely in both directions, they intersect here:
    result.x = line1[0].x + (a * (line1[1].x - line1[0].x));
    result.y = line1[0].y + (a * (line1[1].y - line1[0].y));
    // if line1 is a segment and line2 is infinite, they intersect if:
    if (a > 0 && a < 1) {
      result.onLine1 = true;
    }
    // if line2 is a segment and line1 is infinite, they intersect if:
    if (b > 0 && b < 1) {
      result.onLine2 = true;
    }
    // if line1 and line2 are segments, they intersect if both of the above are true
    return result;
  }

  private getMinRouteForSegment(segment: Point[], middleShapes: Shape[], depth: number = 1): Point[][] {
    if (depth > 10) {
      return [segment];
    }
    if (middleShapes.length) {
      let route = null;
      for (const shape of middleShapes) {
        const calcShape = shape as CalcShape;
        if (this.isLinePlacementIntersect(segment, calcShape.placement)) {
          let lines: Point[][] = calcShape.placement.map((line: DimensionLine) => line.points);

          let nearestLine: Point[] = null;
          let nearestDistance = 0;
          for (const line of lines) {
            const distance = this.distance(segment[0], line[2]);
            if ((!nearestDistance || nearestDistance > distance)
              && !this.isLinePlacementIntersect([segment[0], line[2]], calcShape.selfPlacement)
            ) {
              nearestDistance = distance;
              nearestLine = line;
            }
          }

          lines = lines.filter((line: Point[]) => {
            return (line[0].x !== nearestLine[0].x || line[0].y !== nearestLine[0].y
              || line[1].x !== nearestLine[1].x || line[1].y !== nearestLine[1].y);
          });

          const routes: Point[][][] = [];
          let aRoute = this.buildRoute([segment[0], nearestLine[0]], lines, calcShape, segment[1]);
          routes.push(aRoute);
          aRoute = this.buildRoute([segment[0], nearestLine[1]], lines, calcShape, segment[1]);
          routes.push(aRoute);
          const distances = routes.map(this.routeDistance.bind(this));
          let minDistanceIndex;
          for (const distanceKey in distances) {
            const distance = distances[distanceKey];
            if (!minDistanceIndex || distance < distances[minDistanceIndex]) {
              minDistanceIndex = distanceKey;
            }
          }
          route = routes[minDistanceIndex];
          break;
        }
      }
      if (!route) {
        route = [segment];
      }
      const rMap = route.map((aSegment: Point[]) => this.getMinRouteForSegment(aSegment, middleShapes, depth + 1));
      const rReduce = rMap.reduce((accum, localRoute) => (accum.concat(localRoute)), []);
      return rReduce;
    } else {
      return [segment];
    }
  }

  private distance(point1: Point, point2: Point): number {
    return Math.sqrt(
      Math.pow(Math.abs(point1.x - point2.x), 2)
      + Math.pow(Math.abs(point1.y - point2.y), 2),
    );
  }

  private buildRoute(startLine: Point[], lines: Point[][], shape: CalcShape, dest: Point): Point[][] {
    let cLines = lines;
    const route = [startLine];
    let cPoint = startLine[1];
    while (cLines.length) {
      const lineToDest = [cPoint, dest];
      if (!this.isLinePlacementIntersect(lineToDest, shape.placement)) {
        route.push(lineToDest);
        break;
      } else {
        const cLine = cLines.find((line: Point[]) => {
          return (line[0].x === cPoint.x && line[0].y === cPoint.y
            || line[1].x === cPoint.x && line[1].y === cPoint.y);
        });
        cLines = cLines.filter((line: Point[]) => {
          return (line[0].x !== cLine[0].x || line[0].y !== cLine[0].y
            || line[1].x !== cLine[1].x || line[1].y !== cLine[1].y);
        });
        cPoint = cLine.find((point: Point) => point.x !== cPoint.x || point.y !== cPoint.y);
        route.push(cLine);
      }
    }
    return route;
  }

  private routeDistance(route: Point[][]): number {
    return route.reduce((accum: number, line: Point[]) => (accum + this.distance(line[0], line[1])), 0);
  }

  private replaceShape(shape: Shape) {
    const resisingIndex = this.graph.shapes.findIndex((aShape: Shape) => aShape.name === shape.name);
    const isCovered = (this.graph.shapes[resisingIndex] as CalcRectangle).isCovered;
    this.graph.shapes[resisingIndex] = {
      ...shape as Rectangle,
    };
    (this.graph.shapes[resisingIndex] as CalcRectangle).isCovered = isCovered;
  }
}
