import * as mongoose from 'mongoose';

export const GraphMakingLogSchema = new mongoose.Schema({
  code: { type: String, index: { unique: true } },
  ipAddress: String,
  createdDate: Date,
});
