import { Test, TestingModule } from '@nestjs/testing';
import { GraphController } from './graph.controller';
import { GraphService, graphStubData } from './graph.service';
import * as mongoose from 'mongoose';
import { GraphCodeSequenceSchema } from '../mongoose/graph-code-sequence.schema';
import { getModelToken } from '@nestjs/mongoose';
import { GraphSchema } from '../mongoose/graph.schema';
import { GraphShortDto } from '../dto/graph/graph.short.dto';
import { GraphDto } from '../dto/graph/graph.dto';
import { GraphCodeSequenceService } from './graph-code-sequence.service';
import { GraphCalcClientDataService } from '../graph-calc/graph-calc-client-data.service';
import { GraphMakingLogService } from './graph-making-log.service';
import { GraphMakingLogSchema } from '../mongoose/graph-making-log.schema';
import { Request } from 'express';

const GraphCodeSequence = mongoose.model('GraphCodeSequence', GraphCodeSequenceSchema);
const GraphMakingLogModel = mongoose.model('GraphMakingLog', GraphMakingLogSchema);
const Graph = mongoose.model('Graph', GraphSchema);

describe('Graph Controller', () => {
  let controller: GraphController;
  let graphService: GraphService;
  let calcClientDataService: GraphCalcClientDataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GraphController],
      providers: [
        GraphService,
        GraphCodeSequenceService,
        GraphMakingLogService,
        GraphCalcClientDataService,
        {
          provide: getModelToken('GraphCodeSequence'),
          useValue: GraphCodeSequence,
        },
        {
          provide: getModelToken('GraphMakingLog'),
          useValue: GraphMakingLogModel,
        },
        {
          provide: getModelToken('Graph'),
          useValue: Graph,
        },
        { provide: 'WORKER', useValue: WorkerTesting },
      ],
    }).compile();

    graphService = module.get<GraphService>(GraphService);
    calcClientDataService = module.get<GraphCalcClientDataService>(GraphCalcClientDataService);
    controller = module.get<GraphController>(GraphController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('getGraphList ', async () => {
    const result: GraphShortDto[] = [
      { code: '2a', title: '' },
    ];
    const spy = jest.spyOn(graphService, 'getGraphList').mockImplementation(() => Promise.resolve(result));
    expect(await controller.getGraphList({ search: null, skip: 0 })).toBe(result);
    expect(spy).toHaveBeenCalled();
  });

  it('getGraph ', async () => {
    const result: GraphDto = graphStubData();
    const spy = jest.spyOn(graphService, 'getGraph').mockImplementation(() => Promise.resolve(result));
    expect(await controller.getGraph({ code: '2a' })).toBe(result);
    expect(spy).toHaveBeenCalled();
  });

  it('saveGraph ', async () => {
    calcClientDataService.data['123'] = {
      socket: null,
      graph: graphStubData(),
    };
    const result: GraphDto = graphStubData();
    const spy = jest.spyOn(graphService, 'saveGraph').mockImplementation(() => Promise.resolve(result));
    expect(await controller.saveGraph('123', '<svg></svg>', {ip: '::1'} as Request)).toBe(result);
    expect(spy).toHaveBeenCalled();
  });

  it('reviewGraph ', async () => {
    const spy = jest.spyOn(graphService, 'reviewGraph').mockImplementation(() => Promise.resolve(true));
    expect(await controller.reviewGraph(true, { code: '2a' })).toBe(true);
    expect(spy).toHaveBeenCalled();
  });

  it('removeGraph ', async () => {
    const spy = jest.spyOn(graphService, 'archiveGraph').mockImplementation(() => Promise.resolve(true));
    expect(await controller.removeGraph({ code: '2a' })).toBe(true);
    expect(spy).toHaveBeenCalled();
  });
});

class WorkerTesting {
  /* tslint:disable:no-empty */
  on(message: string, callback: (data: any) => {}) { }
  terminate() { }
}