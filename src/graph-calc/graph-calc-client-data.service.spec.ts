/* tslint:disable:no-string-literal */
import { Test, TestingModule } from '@nestjs/testing';
import { GraphCalcClientDataService } from './graph-calc-client-data.service';
import { listen, Socket } from 'socket.io';
import { connect } from 'socket.io-client';
import { GraphDto } from '../dto/graph/graph.dto';
import { graphStubData } from '../graph/graph.service';
import { CalcTask } from './types/calc-task.interface';
import { WorkerMessage } from './types/worker-message.interface';

describe('GraphCalcClientDataService', () => {
  let service: GraphCalcClientDataService;
  let server;
  let client;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GraphCalcClientDataService,
        { provide: 'WORKER', useValue: WorkerTesting },
      ],
    }).compile();

    service = module.get<GraphCalcClientDataService>(GraphCalcClientDataService);

    server = listen(3002);
    client = connect('http://localhost:3002', {});
  });

  afterEach(async () => {
    if (client && client.connected) {
      client.disconnect();
      client.close();
    }
    server.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return graph by socket', (done) => {
    const graph: GraphDto = graphStubData();
    server.on('connection', (socket: Socket) => {
      try {
        service.getGraph(socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toContain('Graph hasn\'t been initialized yet');
      }

      service.addClient(socket);
      service.initGraph(socket, graph);
      const graph2 = service.getGraph(socket);
      expect(graph).toBe(graph2);

      try {
        socket.id += 'asdf';
        service.getGraph(socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toContain('Graph hasn\'t been initialized yet');
      }

      done();
    });
  });

  it('"addTask" should raise error is socket hasn\'t been initialized', (done) => {
    server.on('connection', (socket: Socket) => {
      try {
        service.addTask(CalcTask.addLink, '', socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toContain('Graph hasn\'t been initialized yet');
      }
      done();
    });
  });

  it('"addTask" should handle worker messages', (done) => {
    TEST_WORKER_TERMINATE_COUNTER = 0;
    const graph: GraphDto = graphStubData();
    server.on('connection', (socket: Socket) => {
      const spy = jest.spyOn(socket, 'emit');
      service.addClient(socket);
      service.initGraph(socket, graph);
      service.addTask(CalcTask.addLink, '', socket);
      expect(spy).toHaveBeenNthCalledWith(1, 'state', 'in_progress');
      const worker = service.data[socket.id].worker;
      expect(service.data[socket.id].worker).toBeTruthy();
      const newGraph: GraphDto = graphStubData();
      newGraph.title = 'NEW TITLE';
      eventHandlers['message']({
        type: WorkerMessage.graph,
        data: {
          success: true,
          graph: newGraph,
        },
      });
      expect(spy).toHaveBeenNthCalledWith(2, 'graph', { success: true, graph: newGraph });

      eventHandlers['message']({
        type: WorkerMessage.outOfBoard,
        data: true,
      });
      expect(spy).toHaveBeenNthCalledWith(3, 'outOfBoard', true);

      const errorMessage = 'New error';
      eventHandlers['message']({
        type: WorkerMessage.exception,
        data: errorMessage,
      });
      expect(spy).toHaveBeenNthCalledWith(4, 'exception', {
        status: 'error',
        message: errorMessage,
      });
      expect(TEST_WORKER_TERMINATE_COUNTER).toBe(3);
      done();
    });
  });

  it('"addTask" should handle worker errors', (done) => {
    const graph: GraphDto = graphStubData();
    server.on('connection', (socket: Socket) => {
      const spy = jest.spyOn(socket, 'emit');
      spy.mockClear();
      service.addClient(socket);
      service.initGraph(socket, graph);
      service.addTask(CalcTask.addLink, '', socket);
      expect(spy).toHaveBeenNthCalledWith(1, 'state', 'in_progress');
      const errorMessage = 'New error';
      eventHandlers['error'](errorMessage);
      expect(spy).toHaveBeenNthCalledWith(2, 'exception', {
        status: 'error',
        message: errorMessage,
      });
      done();
    });
  });

  it('"addTask" should handle worker exit', (done) => {
    const graph: GraphDto = graphStubData();
    server.on('connection', (socket: Socket) => {
      const spy = jest.spyOn(socket, 'emit');
      spy.mockClear();
      service.addClient(socket);
      service.initGraph(socket, graph);
      service.addTask(CalcTask.addLink, '', socket);
      expect(spy).toHaveBeenNthCalledWith(1, 'state', 'in_progress');
      eventHandlers['exit'](0);
      expect(spy).toHaveBeenCalledTimes(2);

      spy.mockClear();
      service.addTask(CalcTask.addLink, '', socket);
      eventHandlers['exit'](1);
      expect(spy).toHaveBeenCalledTimes(3);
      expect(spy).toHaveBeenNthCalledWith(3, 'state', 'pending');
      done();
    });
  });

  it('"addTask" should add task to existing worker', (done) => {
    const graph: GraphDto = graphStubData();
    server.on('connection', (socket: Socket) => {
      service.addClient(socket);
      service.initGraph(socket, graph);
      service.addTask(CalcTask.addLink, '', socket);
      service.addTask(CalcTask.addLink, '', socket);
      expect(TEST_WORKER_MESSAGES_COUNTER).toBe(1);
      done();
    });
  });

  it('"breakCalculations" should raise error is socket hasn\'t been initialized', (done) => {
    server.on('connection', (socket: Socket) => {
      try {
        service.breakCalculations(socket);
        done.fail('Unexpected behavior');
      } catch (e) {
        expect(e.message).toContain('Graph hasn\'t been initialized yet');
      }
      done();
    });
  });

  it('"breakCalculations" should terminate worker', (done) => {
    const graph: GraphDto = graphStubData();
    TEST_WORKER_TERMINATE_COUNTER = 0;
    server.on('connection', (socket: Socket) => {
      const spy = jest.spyOn(socket, 'emit');
      spy.mockClear();
      service.addClient(socket);
      service.initGraph(socket, graph);
      service.addTask(CalcTask.addLink, '', socket);
      service.breakCalculations(socket);
      expect(spy).toHaveBeenNthCalledWith(2, 'graph', {
        success: false,
        graph: service.data[socket.id].graph,
      });
      expect(TEST_WORKER_TERMINATE_COUNTER).toBe(1);
      done();
    });
  });
});

let TEST_WORKER_TERMINATE_COUNTER = 0;
let TEST_WORKER_MESSAGES_COUNTER = 0;
const eventHandlers = {};
class WorkerTesting {
  on(message: string, callback: (data: any) => {}) {
    eventHandlers[message] = callback;
  }
  terminate() {
    TEST_WORKER_TERMINATE_COUNTER++;
  }
  postMessage(data) {
    TEST_WORKER_MESSAGES_COUNTER++;
  }
}
