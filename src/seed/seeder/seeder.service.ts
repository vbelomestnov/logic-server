import { Injectable, Logger } from '@nestjs/common';
import { UserService } from '../../user/user.service';

@Injectable()
export class SeederService {
    constructor(
        private readonly logger: Logger,
        private readonly userService: UserService,
    ) { }

    async seed() {
        await this.userService.create('admin', '^yxe3$Kqn$7eFbGdg6C');
        this.logger.debug('Successfuly completed seeding users...');
    }
}
