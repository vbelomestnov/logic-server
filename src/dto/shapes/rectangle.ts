import { Shape } from './shape';
import { IsNumber, Min } from 'class-validator';

export class Rectangle extends Shape {
  @IsNumber()
  @Min(0)
  height: number;

  @IsNumber()
  @Min(0)
  width: number;
}
