export enum CalcTask {
  addShape,
  removeShape,
  addLink,
  removeLink,
  setShapePlacements,
  saveTitle,
  saveShapeText,
}
