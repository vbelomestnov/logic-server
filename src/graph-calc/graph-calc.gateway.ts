import { SubscribeMessage, WebSocketGateway, MessageBody, ConnectedSocket, OnGatewayConnection, OnGatewayDisconnect, WsException } from '@nestjs/websockets';
import { GraphService } from '../graph/graph.service';
import { GraphCalcClientDataService } from './graph-calc-client-data.service';
import { Socket } from 'socket.io';
import { Rectangle } from '../dto/shapes/rectangle';
import { RemoveLinkParams } from './types/remove-link-params.interface';
import { Link } from '../dto/shapes/link';
import { CalcTask } from './types/calc-task.interface';
import { SaveShapeTextParams } from './types/save-shape-text-params';
import { ValidationPipe, UsePipes } from '@nestjs/common';

@WebSocketGateway(4201)
// Return "Internal server error" instead of validation details
// @UsePipes(new ValidationPipe({transform: true}))
export class GraphCalcGateway implements OnGatewayConnection, OnGatewayDisconnect {

  constructor(
    private readonly graphService: GraphService,
    private readonly calcDataService: GraphCalcClientDataService,
  ) { }

  handleConnection(client: Socket) {
    this.calcDataService.addClient(client);
    client.emit('state', 'empty');
  }

  handleDisconnect(client: Socket) {
    this.calcDataService.removeClient(client);
  }

  @SubscribeMessage('initGraph')
  async handleInitGraph(@MessageBody() code: string, @ConnectedSocket() socket: Socket) {
    try {
      const graph = await this.graphService.getGraph(code);
      this.calcDataService.initGraph(socket, graph);
      socket.emit('state', 'pending');
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  @SubscribeMessage('addShape')
  async handleAddShape(@MessageBody() shape: Rectangle, @ConnectedSocket() socket: Socket) {
    try {
      this.calcDataService.addTask(CalcTask.addShape, shape, socket);
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  @SubscribeMessage('removeShape')
  async handleRemoveShape(@MessageBody() shapeName: string, @ConnectedSocket() socket: Socket) {
    try {
      this.calcDataService.addTask(CalcTask.removeShape, shapeName, socket);
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  @SubscribeMessage('addLink')
  async handleAddLink(@MessageBody() link: Link, @ConnectedSocket() socket: Socket) {
    try {
      this.calcDataService.addTask(CalcTask.addLink, link, socket);
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  @SubscribeMessage('removeLink')
  async handleRemoveLink(@MessageBody() removeLinkParams: RemoveLinkParams, @ConnectedSocket() socket: Socket) {
    try {
      this.calcDataService.addTask(CalcTask.removeLink, removeLinkParams, socket);
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  @SubscribeMessage('setShapePlacements')
  async handleSetShapePlacement(@MessageBody() shape: Rectangle, @ConnectedSocket() socket: Socket) {
    try {
      this.calcDataService.addTask(CalcTask.setShapePlacements, shape, socket);
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  @SubscribeMessage('saveTitle')
  async handleSaveTitle(@MessageBody() title: string, @ConnectedSocket() socket: Socket) {
    try {
      this.calcDataService.addTask(CalcTask.saveTitle, title, socket);
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  @SubscribeMessage('saveShapeText')
  async handleSaveShapeText(@MessageBody() saveShapeTextParams: SaveShapeTextParams, @ConnectedSocket() socket: Socket) {
    try {
      this.calcDataService.addTask(CalcTask.saveShapeText, saveShapeTextParams, socket);
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  @SubscribeMessage('break')
  async handleBreakCalculatoins(@ConnectedSocket() socket: Socket) {
    try {
      this.calcDataService.breakCalculations(socket);
    } catch (e) {
      throw new WsException(e.message);
    }
  }
}
