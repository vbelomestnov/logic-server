import { IsString, Length, IsAlphanumeric, ValidateNested, IsArray, ArrayMinSize, IsNumber, Max, Min } from 'class-validator';
import { Type } from 'class-transformer';
import { Rectangle } from '../shapes/rectangle';
import { Link } from '../shapes/link';
import { Embedded } from '../shapes/embedded';

export class GraphDto {
  @IsString()
  @Length(1)
  @IsAlphanumeric()
  code: string;

  @IsString()
  @Length(10)
  title: string;

  @IsNumber()
  @Max(1000)
  @Min(0)
  width: number;

  @IsNumber()
  @Max(1000)
  @Min(500)
  height: number;

  @ValidateNested({ each: true })
  // @Type(() => Rectangle)
  // @Type(() => Embedded)
  @IsArray()
  @ArrayMinSize(1)
  shapes: Array<Rectangle | Embedded>;

  @ValidateNested({ each: true })
  @Type(() => Link)
  @IsArray()
  links: Link[];
}
