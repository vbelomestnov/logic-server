import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { connect } from 'socket.io-client';

describe('AppController (e2e)', () => {

  it('socket connect', (done) => {
    jest.setTimeout(400000);
    const clients = [];
    const hrstart = process.hrtime();
    for (let index = 0; index < CLIENTS_AMOUNT; index++) {
      setTimeout(() => {
        const client = connect('https://impacton.world', {});
        client.once('connect', () => {
          console.log(`CONNECT ${client.id}`);
          expect(client.connected).toBe(true);
          client.emit('initGraph', 'new');
        });

        client.on('state', (state) => {
          if (state === 'pending') {
            client.emit('addShape', newShapeStub());
          }
        });

        client.once('graph', () => {
          clients.push(client);
          console.log(`NEW SHAPE #${clients.length} ${client.id}`);

          if (clients.length >= CLIENTS_AMOUNT) {

            let disconnectedCounter = 0;
            for (const closeClient of clients) {
              closeClient.once('disconnect', () => {
                disconnectedCounter++;
                console.log(`DISCONNECT #${clients.length - disconnectedCounter}`);
                expect(closeClient.disconnected).toBe(true);
                if (disconnectedCounter === CLIENTS_AMOUNT) {
                  const hrend = process.hrtime(hrstart);
                  setTimeout(() => {
                    console.log('[socket connect] Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000);
                    done();
                  }, 5000);
                }
              });
              setTimeout(() => {
                closeClient.disconnect();
              }, Math.trunc(Math.random() * 100) * 1000);
            }
          }
        });
      }, Math.trunc(Math.random() * 100) * 1000);
    }
  });
});

const CLIENTS_AMOUNT = 500;

const newShapeStub = () => {
  return {
    height: 50,
    name: 'object-1',
    placement: [],
    selfPlacement: [],
    text: 'asdf asdf sdfrwet',
    type: 0,
    width: 100,
    x: 162,
    y: 122,
  };
};
