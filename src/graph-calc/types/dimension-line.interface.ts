import { LineType } from './line-type.interface';
import { Side } from './side.interface';
import { Point } from '../../dto/shapes/point';

export interface DimensionLine {
  type: LineType;
  side: Side;
  points: Point[];
}
