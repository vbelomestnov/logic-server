import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UserService,
    private readonly jwtService: JwtService,
  ) { }

  async validateUser(login: string, password: string): Promise<any> {
    const user = await this.usersService.findOne(login);
    if (user && await compare(password, user.password)) {
      delete user.password;
      return user;
    }
    return null;
  }

  async login(user: any) {
    const payload = { login: user.login, sub: user._id.toHexString() };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
